package ar.edu.unq.po2.tp3;

import java.util.ArrayList;
import java.time.LocalDate;

public class EquipoDeTrabajo {

	private String nombre;
	private ArrayList<Persona> integrantes;
	
	public EquipoDeTrabajo(String nombre) {
		this.nombre = nombre;
		this.integrantes = new ArrayList<Persona>();
	}

	public String getNombre() {
		return nombre;
	}

	public float promedioEdad() {
		return (float) integrantes.stream().map(Persona::getEdad).reduce(0,(a, b) -> a + b)  
					/ integrantes.stream().count();
	}
	
	public void agregarIntegrante(Persona integrante) {
		integrantes.add(integrante);
	}


	
	public static void main(String[] args) {
		
		Persona jorge  = new Persona("Jorge",LocalDate.of(1990, 2, 24));
		Persona maria  = new Persona("Mar�a",LocalDate.of(1991, 6, 13));
		Persona brenda = new Persona("Brenda",LocalDate.of(2000, 1, 24));
		Persona nahuel = new Persona("Nahuel",LocalDate.of(1980, 4, 1));
		Persona matias = new Persona("Mat�as",LocalDate.of(1990, 10, 30));
		
		EquipoDeTrabajo best = new EquipoDeTrabajo("Best");
		
		best.agregarIntegrante(matias);
		best.agregarIntegrante(nahuel);
		best.agregarIntegrante(brenda);
		best.agregarIntegrante(maria);
		best.agregarIntegrante(jorge);
		
		System.out.println("El promedio de edad del equipo es: " + best.promedioEdad());	
	}
	
}
