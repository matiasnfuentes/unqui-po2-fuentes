package ar.edu.unq.po2.tp3;
import java.time.LocalDate;
import java.time.Period;


/**
 * Si un objeto cualquiera que le pide la edad a una Persona: �Conoce c�mo �sta calcula u obtiene tal valor? 
 * �C�mo se llama el mecanismo de abstracci�n que permite esto?
 * 
 * Si un objeto cualquiera le pide la edad a la persona, est� la calcula mediante el m�todo correspondiente y la retorna.
 * El mecanismo de abstracci�n que permite esto es... ???
 *   
 * @author Matias
 *
 */
public class Persona {
	
	private String nombre;
	private LocalDate fechaNacimiento;
	
	public Persona(String nombre, LocalDate fechaNacimiento) {
		this.nombre = nombre;
		this.fechaNacimiento = fechaNacimiento;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public LocalDate getFechaNacimiento() {
		return fechaNacimiento;
	}
	public void setFechaNacimiento(LocalDate fechaNacimiento) {
		this.fechaNacimiento = fechaNacimiento;
	}
	
	public int getEdad() {
		return Period.between(fechaNacimiento, LocalDate.now()).getYears();
	}
	
	public boolean menorQue(Persona persona) {
		return persona.getEdad()<this.getEdad();
	}

}
