package ar.edu.unq.po2.tp3;

import java.util.ArrayList;


public class Multioperador {
	
	private ArrayList<Integer> numeros;
	
	public int suma(){
		int total = 0;
		for(int numero: numeros) {
			total+=numero;
		}
	return total;
	}
	
	public int resta(){
		int total = 0;
		for(int numero: numeros) {
			total-=numero;
		}
	return total;
	}
	
	public int multiplicacion(){
		int total = 0;
		for(int numero: numeros) {
			total*=numero;
		}
	return total;
	}
	

}
