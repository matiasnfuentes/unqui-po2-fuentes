package ar.edu.unq.po2.tp3;

/** 
 * 
 * @author Matias
 * 
 * 1. Que son los tipos de datos primitivos?
   Los tipos de datos primitivos son tipos de datos predefinidos por el lenguaje y 
   nombrados con una palabra clave.

   2. Cual es la diferencia entre un int y un Integer?

   La diferencia entre ambos es que "int" ser refiere al tipo primitivo predefinido por java,
   en cambio un Integer es una clase de Java comunmente llamada "Wrapper" o clase envoltorio.
   Esta clase solo tiene una variable de tipo int, convirtiendo al tipo primitivo en un objeto.
   Al convertirlo en un objeto nos permite enviarle diversos mensajes de gran utilidad.

   3. Si se define una variable de instancia de tipo int, cual ser�a su valor predeterminado?
   Y si se define tipo integer? Haga la prueba en Eclipse.
    
   Los valores por defecto ser�an:
    * int     = 0
    * Integer = null

   4. Responder la pregunta del punto anterior(3), pero ahora en lugar de definir una variable
   de instancia se define una variable en el metodo.
   
   Las variables de los metodos no tienen valor por defecto, deben ser inicializadas o asignadas por el usuario
 *
 */

public class TiposPrimitivos {
	private int intClase;
	private Integer integerClase;
	
	public static void main(String[] args) {
		
		TiposPrimitivos tipoPrimitivo = new TiposPrimitivos();
		int intMethod;
		int integerMethod;
		
		System.out.println(tipoPrimitivo.intClase);
		System.out.println(tipoPrimitivo.integerClase);
		
		//Las variables de los metodos no tienen valor por defecto.
		// Deben ser inicializadas por el usuario.
		//System.out.println(intMethod);
		//System.out.println(integerMethod);
	}

}
