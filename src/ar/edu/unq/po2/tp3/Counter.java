package ar.edu.unq.po2.tp3;
import java.util.ArrayList;

public class Counter {
	
	private ArrayList<Integer> numbers;
	
	public Counter() {
		numbers = new ArrayList<Integer>();
	}
	
	public void addNumber(int num) {
		numbers.add(num);
	}
	
	public int getEvenOcurrences(){
		return (int) numbers.stream().filter(n-> (n % 2) == 0).count();
	}
	
	public int getOddOcurrences(){
		return (int) numbers.stream().filter(n-> (n % 2) != 0).count();
	}
	
	public int getMultiples(int num){
		return (int) numbers.stream().filter(n-> n % num == 0).count();
	}

}
