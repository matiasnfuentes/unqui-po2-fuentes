package ar.edu.unq.po2.tp3;

public class Point {
	private float cordenadaX;
	private float cordenadaY;
	
	public Point() {
		this.cordenadaX = 0;
		this.cordenadaY = 0;
	}
	
	public Point(float cordenadaX, float cordenadaY) {
		this.cordenadaX = cordenadaX;
		this.cordenadaY = cordenadaY;
	}
	
	public void moverPunto(float cordenadaX, float cordenadaY) {
		this.cordenadaX = cordenadaX;
		this.cordenadaY = cordenadaY;
	}
	
	public Point sumarPuntos(Point punto) {
		return new Point(punto.getCordenadaX()+this.getCordenadaX(),punto.getCordenadaY()+this.getCordenadaY());
	}

	public float getCordenadaX() {
		return cordenadaX;
	}

	public float getCordenadaY() {
		return cordenadaY;
	}

}
