package ar.edu.unq.po2.tp3;

public class Rectangulo {
	
	private float ancho;
	private float alto;
	private Point bordeSuperiorIzquierdo;
	
	public Rectangulo(Point bordeSuperiorIzquierdo,float ancho, float alto){
		this.ancho = ancho;
		this.alto = alto;
		this.bordeSuperiorIzquierdo = bordeSuperiorIzquierdo; 
	}
	
	public float getAncho() {
		return ancho;
	}

	public float getAlto() {
		return alto;
	}

	public Point getBordeSuperiorIzquierdo() {
		return bordeSuperiorIzquierdo;
	}

	public void setAncho(float ancho) {
		this.ancho = ancho;
	}

	public void setAlto(float alto) {
		this.alto = alto;
	}

	public void setBordeSuperiorIzquierdo(Point bordeSuperiorIzquierdo) {
		this.bordeSuperiorIzquierdo = bordeSuperiorIzquierdo;
	}
	
	public float area() {
		return this.getAncho() * this.getAlto();
	}
	
	public float perimetro() {
		return this.getAncho() * 2 +  this.getAlto() * 2;
	}
	
	public boolean esHorizontal() {
		return this.getAncho()> this.getAlto();
	}

}
