package ar.edu.unq.po2.tp3;

public class ExaminarLasExpresiones {
	
	public static void main(String[] args) {
		String a = "abc";
		String s = a;
		String t;
		
		s.length();
		// Retorna 3
		
		// System.out.println(t.length()); 
		// No es valido, primero hay que inicializar la variable
			
		// 1 + a;
		// Para que la expresi�n sea v�lida debe estar contenida en una variable
		// o en un return.
		// En ese caso el resultado de la misma es "1abc"
		
		a.toUpperCase();
		// Retorna "ABC"
		
		"Libertad".indexOf("r");
		// Retorna 4
		
		"Universidad".lastIndexOf('i');
		// Retorna 7
		
		"Quilmes".substring(2,4);
		// Retorna "il"
		
		(a.length() + a).startsWith("a");
		// Retorna false
		
		// s==a;
		// Para que sea valido debe estar asignado a una variable, o a un return
		// o ser condicion en una estructura de control. 
		// En ese caso el resultado de la misma es true;
		
		a.substring(1,3).equals("bc");
		// Retorna true
		
	}

}

