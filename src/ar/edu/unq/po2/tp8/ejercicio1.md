# Ejercicio 6 - Explique y desarrolle el significado de los siguientes postulados de TDD y Test de Unidad.
## 1. Mantener en forma exhaustiva una suite de tests. 
    Lo que se quiere decir con este principio es que los test deben cubrir la totalidad del código del SUT, y cada una de sus posibilidades. Es decir, se debe testear todo lo que se tenga que testear y esos test deben estar actualizados. 
## 2. No deben utilizarse para testear otros objetos del dominio.
    Cuando realizamos test a una parte del dominio, nos tenemos que centrar en solo testear esa parte. Si una parte de nuestro código tiene interacciones o dependencia con otras clases, lo que tenemos que lograr es aislar a esa clase y tener independencia entre los test. De esta manera nos aseguramos testear una clase a la vez, y si nuestros test fallan, sabemos que es un problema de esa clase en si misma, y no de otra clase que interactua con ella.
## 3. Comunicar la intención del test
    Lo que se busca es que el test sea facíl de entender a simple vista. Se trata de lograr que a penas se lea el test, se sepa que se quiere testear y de que forma. Hay que evitar que los test tengan código en exceso o estructuras condicionales, ya que esto provoca que el test sea dificil de entender. Si hacemos explicito y claro lo que queremos testear, hacemos que el test sea mas facíl de entender y de mantener.

