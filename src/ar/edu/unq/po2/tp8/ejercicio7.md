# Ejercicio 6 - Test Doubles
## 1) ¿Qué son los test doubles?
Los test dobles son cualquier objeto o componente que se instala en lugar del componente real, con el proposito expreso de correr un test. Es un tipo de objeto que es mucho mas cooperativo y nos permite escribir los test en la manera que nosotros queremos hacerlo.

## 2) Enumere los tipos de test doubles y de ejemplos concretos de uso. Indique como usaría, cuando es posible, mockito para emular los diferentes tipos de tipos de test doubles.


#### 1. Dummy:

    Es un objeto que solo ocupa lugar, y se pasa al SUT como argumento, pero nunca es realmente usado. Se usa normalmente cuando tenemos un DOC que afecta al SUT, pero con el que no vamos a interactuar. Por ejemplo, tenemos la ClaseA, cuyo constructor tiene la firma: 
    
        Public ClaseA(ClaseB claseB);
    
    Por lo tanto necesitamos emular ese objeto ClaseB para poder hacer nuestras pruebas de forma independiente. Esto lo podemos realizar con Mockito a traves de la siguiente sentencia:
    
        ClaseB claseB = mock(ClaseB.class);
        
#### 2. Stub:
    
    Es un objeto que reemplaza al componente real del cual el SUT depende, para así poder controlar los inputs indirectos del mismo.Tiene dos "subclasificaciones":
        - Responder: Es un tipo de test Stub usado para injectar inputs valido o invalidos  al sut, mediante retornos ante los llamados a metodos
        - Saboteur: Es otra clase de test strub que levanta excepciones o errores para injectar inputs indirectos al SUT.
        
    Por ejemplo en el siguiente código:
    
        Clase A{
            
            public void ejecutar(ClaseB uno, ClaseB dos){
                if (uno.cumple()){
                    uno.ejecutar();
                }
                else{
                    dos.ejecutar
                }
            }
            
        }
        
    En este caso, podemos mockear los objetos de la ClaseB para que nos respondan lo que necesitamos ante el método "cumple" y así poder testear ambas ramas del código. Con mockito lo podemos hacer de la siguiente manera:
    
        ClaseB uno = mock(ClaseB.Class)
        when(uno.cumple()).thenReturn(True);
        
    De esta manera nos aseguramos que cuando se llame al método cumple() con el mock "uno", la respuesta siempre será True.
    
#### 3. Mock Object:
    
    Un Mock Object es un objeto que reemplaza al componente real del cual el sut depende para poder testear y verificar sus outputs indirectos. 
    
    Por ejemplo en el siguiente código:
    
        Clase A{
            public void ejecutar(ClaseB uno){
                uno.ejecutar();
            }
        }
        
    Quiero testear que al objeto "uno" le llego el mensaje "ejecutar", y con mockito lo puedo hacer de la siguiente manera:
    
    ClaseA claseA = new ClaseA();
    ClaseB uno = mock(ClaseB.Class);
    claseA.ejecutar(uno);
    verify(uno).ejecutar();
    
    A través del comando "verify" se verifica que el Mock object recibió el output indirecto esperado.
    
#### 4 . Test Spy
    El Test Spy es muy similar a un Mock object. Actua como un punto de observación para los outputs indirectos del SUT. Agrega la capacidad de llevar un registro de las llamadas hechas a sus métodos por el SUT. Cuando se realiza una llamada a un método de un spy, se llama a los métodos de la clase real.
    
    Por ejemplo:
    
        List<String> lista = new ArrayList<String>();
        List<String> spyLista = Mockito.spy(lista);
     
        spyLista.agregar("uno");
        spyLista.agregar("dos");
     
        Mockito.verify(spyLista).agregar("uno");
        Mockito.verify(spyLista).agregar("dos");
     
        assertEquals(2, spyLista.size());
        
