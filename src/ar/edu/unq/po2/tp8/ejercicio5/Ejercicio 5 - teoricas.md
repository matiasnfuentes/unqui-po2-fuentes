// Ejercicio 5 - teoricas

//2. ¿Cómo se indica en mockito que el objeto mock debe recibir un secuencia de mensajes particular en un orden
// preestablecido? Agregue un ejemplo.

/* Para verificar que un mock recibe una serie de mensajes  en un orden particular se debe usar 
un objeto de tipo "InOrder" y la instruccion verify de la siguiente manera*/

Object objetoMockeado = mock(Object.class);
objectoMockeado.mensaje1;
objectoMockeado.mensaje2;
objectoMockeado.mensaje3;
 
InOrder inOrder = Mockito.inOrder(objetoMockeado);
inOrder.verify(objetoMockeado).mensaje1;
inOrder.verify(objetoMockeado).mensaje2;
inOrder.verify(objetoMockeado).mensaje3;

/*3. ¿Cómo hacer para que un objeto mock este preparado para recibir algunos mensajes sin importar el orden o la
obligatoriedad de recibirlos? Agregue un ejemplo. */

/* Los objetos mockeados pueden recibir todos los mensajes de la clase que mockean, sin necesidad de incorporar 
ninguna instruccion, pero si se quiere que tengan una respuesta especifica se debe agregar una instruccion adicional
despues de mockear el objeto, por ejemplo: */

Object objetoMockeado = mock(Object.class);
when(objetoMockeado.mensaje1()).thenReturn("hola");

// En este caso cuando se invoque al mensaje1 del objectoMockeado, el mismo retornara "hola".

// 4. Es posible anidar envío de mensajes con mockito. Si es posible, como se hace.

// No se a que se refiere.

// 5. ¿Como es la forma de verificación con mockito?

/* El modo de verificacion con Mockito es a traves de la sentencia "verify", que tiene varias opciones
depende de lo que estemos buscando*/