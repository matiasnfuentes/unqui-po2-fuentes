package ar.edu.unq.po2.tp8.ejercicio4;

public class CartaDePoquer {
	
	private ValorDeCarta valor;
	private Palo palo;
	
	public CartaDePoquer(ValorDeCarta valor,Palo palo) {
		this.valor = valor;
		this.palo = palo;
	}

	public Palo getPalo() {
		return palo;
	}

	public ValorDeCarta getValor() {
		return valor;
	}
	
	public boolean esMayorQue(CartaDePoquer cartaAComparar) {
		return this.getValor().esMayorQue(cartaAComparar.getValor());
	}
	
	public boolean poseeMismoPalo(CartaDePoquer cartaAComparar) {
		return this.getPalo().equals(cartaAComparar.getPalo());
	}
	
	public boolean esIgualA(CartaDePoquer cartaAComparar) {
		return this.getValor().equals(cartaAComparar.getValor());
	}

}


