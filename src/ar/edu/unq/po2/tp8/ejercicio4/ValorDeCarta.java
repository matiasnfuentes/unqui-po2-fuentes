package ar.edu.unq.po2.tp8.ejercicio4;

public enum ValorDeCarta {
	DOS,TRES,CUATRO,CINCO,SEIS,SIETE,OCHO,NUEVE,DIEZ,J,Q,K,AS;
	
	public boolean esMayorQue(ValorDeCarta valor){
		return this.ordinal() > valor.ordinal();
	}
}
