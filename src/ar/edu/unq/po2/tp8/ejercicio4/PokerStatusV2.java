package ar.edu.unq.po2.tp8.ejercicio4;

import java.util.List;
import ar.edu.unq.po2.tp8.ejercicio4.jugadaDePoquer.JugadaDePoquer;

public class PokerStatusV2 {
	
	private String resultado;
	private List<JugadaDePoquer> jugadas;
	
	public PokerStatusV2(List<JugadaDePoquer> jugadas) {
		this.resultado = "Nada";
		this.jugadas = jugadas;
	}
	
	public String getResultado() {
		return resultado;
	}

	public void setResultado(String resultado) {
		this.resultado = resultado;
	}
	
	public String verificar(CartaDePoquer carta1, CartaDePoquer carta2, CartaDePoquer carta3,
							CartaDePoquer carta4, CartaDePoquer carta5) {
		
		String resultado = "Nada"; 
		for (JugadaDePoquer jugada : jugadas) {
			if (jugada.verificarJugada(carta1,carta2,carta3,carta4,carta5,this)!="Nada") {
				resultado = jugada.verificarJugada(carta1,carta2,carta3,carta4,carta5,this);
			}
		}
		return resultado;
	}

}
