package ar.edu.unq.po2.tp8.ejercicio4.jugadaDePoquer;

import java.util.List;

import ar.edu.unq.po2.tp8.ejercicio4.CartaDePoquer;
import ar.edu.unq.po2.tp8.ejercicio4.PokerStatusV2;

public class Color extends JugadaDePoquer {

	@Override
	public String verificarJugada(CartaDePoquer carta1, CartaDePoquer carta2, CartaDePoquer carta3, 
								CartaDePoquer carta4,CartaDePoquer carta5,PokerStatusV2 pokerStatus) {
		
		List<CartaDePoquer> cartas = List.of(carta1,carta2,carta3,carta4,carta5);
		String resultado = "Nada";
		
		if (cartas
					.stream()
					.filter(c -> c.poseeMismoPalo(carta1))
					.count() == 5) {
			
			resultado = "Color";
		}
		return resultado;
	}
	
}
