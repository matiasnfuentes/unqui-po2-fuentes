package ar.edu.unq.po2.tp8.ejercicio4.jugadaDePoquer;

import ar.edu.unq.po2.tp8.ejercicio4.CartaDePoquer;
import ar.edu.unq.po2.tp8.ejercicio4.PokerStatusV2;

public class Poquer extends JugadaDePoquer {

	@Override
	public String verificarJugada(CartaDePoquer carta1, CartaDePoquer carta2, CartaDePoquer carta3, 
									CartaDePoquer carta4,CartaDePoquer carta5,PokerStatusV2 pokerStatus) {
		String resultado = "Nada";
		
		if (this.cantidadDeCartasIguales(carta1,carta2,carta3,carta4,carta5) == 4) {
			resultado ="Poquer";
		}
		
		return resultado;
	}

}
