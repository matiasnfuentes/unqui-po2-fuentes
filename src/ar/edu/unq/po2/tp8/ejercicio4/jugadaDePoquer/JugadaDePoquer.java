package ar.edu.unq.po2.tp8.ejercicio4.jugadaDePoquer;

import java.util.List;
import ar.edu.unq.po2.tp8.ejercicio4.CartaDePoquer;
import ar.edu.unq.po2.tp8.ejercicio4.PokerStatusV2;

public abstract class JugadaDePoquer {
	
	public long cantidadDeCartasIguales(CartaDePoquer carta1, CartaDePoquer carta2, 
					CartaDePoquer carta3,CartaDePoquer carta4, CartaDePoquer carta5) {

		long cantidadDecartasIguales = 0;
		List<CartaDePoquer> cartas = List.of(carta1,carta2,carta3,carta4,carta5);
		
		for (CartaDePoquer carta : cartas) {
			long igualesAEstaCarta = cartas
										.stream()
										.filter(c -> c.esIgualA(carta))
										.count();
			if (igualesAEstaCarta > cantidadDecartasIguales) {
				cantidadDecartasIguales = igualesAEstaCarta;
			}
		}
		return cantidadDecartasIguales; 
	}
	
	public abstract String verificarJugada(CartaDePoquer carta1, CartaDePoquer carta2, CartaDePoquer carta3,
										 CartaDePoquer carta4, CartaDePoquer carta5,PokerStatusV2 pokerStatus);

}
