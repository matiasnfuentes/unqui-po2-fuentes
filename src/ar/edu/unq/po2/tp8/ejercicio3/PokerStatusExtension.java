package ar.edu.unq.po2.tp8.ejercicio3;

import java.util.List;

public class PokerStatusExtension {
	
	public String verificar(String carta1, String carta2, String carta3,
			                                String carta4, String carta5) {
		
		String jugada = "Nada";
		
		if(this.hayColor(carta1, carta2, carta3, carta4, carta5)) {
			jugada = "Color";
		}
		
		if(this.hayPoquer(carta1, carta2, carta3, carta4, carta5)) {
			jugada = "Poquer";
		}
		
		if(this.hayTrio(carta1, carta2, carta3, carta4, carta5)) {
			jugada = "Trio";
		}
		
		return jugada;
	} // Investigar Patr�n command.
	
	private boolean hayColor(String carta1, String carta2, String carta3, String carta4, String carta5) {
		
		String palo = carta1.substring(carta1.length()-1);
		List<String> cartas = List.of(carta1,carta2,carta3,carta4,carta5);
		return cartas
				.stream()
				.filter(c -> c.contains(palo))
				.count() == 5;
		
	}


	public boolean hayPoquer(String carta1, String carta2, String carta3,
							String carta4, String carta5){
		
			return this.cantidadDeCartasIguales(carta1,carta2,carta3,carta4,carta5) == 4; 
	}
	
	private boolean hayTrio(String carta1, String carta2, String carta3, String carta4, String carta5) {
		
		return this.cantidadDeCartasIguales(carta1,carta2,carta3,carta4,carta5) == 3; 
	}
	
	private long cantidadDeCartasIguales(String carta1, String carta2, String carta3,
			String carta4, String carta5) {
		
			long cantidadDecartasIguales = 0;
			List<String> cartas = List.of(carta1,carta2,carta3,carta4,carta5);
			for (String carta : cartas) {
				long igualesAEstaCarta = cartas
											.stream()
											.filter(c -> this.compararDosCartas(c, carta))
											.count();
				if (igualesAEstaCarta>cantidadDecartasIguales) {
					cantidadDecartasIguales = igualesAEstaCarta;
				}
			}
			return cantidadDecartasIguales; 
	}
	
	private boolean compararDosCartas(String carta1,String carta2) {
		
		int longitud = carta1.length();
		
		if(longitud==carta2.length()) {
			return(carta1.substring(0, longitud-1).equals(carta2.substring(0, longitud-1)));
		}
		else {return false;}
	}
	
	
}
