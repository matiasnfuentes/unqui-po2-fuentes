package ar.edu.unq.po2.tp8.ejercicio2;

import java.util.List;

public class PokerStatus {
	
	public boolean verificar(String carta1, String carta2, String carta3,
			                                String carta4, String carta5) {
		
		List<String> cartas = List.of(carta1,carta2,carta3,carta4,carta5);
		long cantidadDecartasIguales = Long.max(cartas.stream().filter(c -> this.compararDosCartas(c, carta1)).count(),
				  								cartas.stream().filter(c -> this.compararDosCartas(c, carta2)).count());
		return cantidadDecartasIguales == 4; 
	}
	
	private boolean compararDosCartas(String carta1,String carta2) {
		
		int longitud = carta1.length();
		
		if(longitud==carta2.length()) {
			return(carta1.substring(0, longitud-1).equals(carta2.substring(0, longitud-1)));
		}
		else {return false;}
	}
	
	}
