package ar.edu.unq.po2.tp9.observer.ejercicio3;

import java.util.ArrayList;

public class Notificador {
	
	private ArrayList<Participante> observadores;

	public Notificador(ArrayList<Participante> participantes) {
		this.observadores = participantes;
	}
	
	public void agregarObservador(Participante participante) {
		this.observadores.add(participante);
	}
	
	public void eliminarObservador(Participante participante) {
		this.observadores.remove(participante);
	}
	
	public void notificarObservadores(String informe) {
		this.observadores.stream().forEach(p -> p.update(informe));
	}
	
	public void notificarObservadores(ArrayList<String> preguntas) {
		this.observadores.stream().forEach(p -> p.update(preguntas));
	}
	
	public void notificarObservador(Participante participante,String informe) {
		this.observadores.stream().findFirst().ifPresent(p -> p.update(informe));
	}

}
