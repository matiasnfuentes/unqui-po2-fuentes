package ar.edu.unq.po2.tp9.observer.ejercicio3.estadosDeLaPartida;

import ar.edu.unq.po2.tp9.observer.ejercicio3.Participante;
import ar.edu.unq.po2.tp9.observer.ejercicio3.Partida;
import ar.edu.unq.po2.tp9.observer.ejercicio3.PreguntaYRespuesta;

public class Finalizada implements EstadoDeLaPartida{

	@Override
	public void verificarRespuesta(Partida partida, Participante participante, PreguntaYRespuesta preguntaYRespuesta) {
		System.out.println("La partida esta finalizada, no se pueden verificar mas respuestas");
	}

	@Override
	public void solicitarUnirse(Partida partida, Participante participante) {
		System.out.println("La partida esta finalizada, no se pueden agregar jugadores");
	}

	@Override
	public void finalizarPartida(Partida partida) {
		System.out.println("La partida ya esta finalizada");
	}

}
