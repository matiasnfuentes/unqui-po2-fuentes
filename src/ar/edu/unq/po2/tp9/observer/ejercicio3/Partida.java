package ar.edu.unq.po2.tp9.observer.ejercicio3;

import java.util.ArrayList;
import java.util.Map;

import ar.edu.unq.po2.tp9.observer.ejercicio3.estadosDeLaPartida.EstadoDeLaPartida;

public class Partida extends Notificador {
	
	private ArrayList<PreguntaYRespuesta> preguntasYRespuestas; 
	private Map<Participante,ArrayList<PreguntaYRespuesta>> participantes;
	private EstadoDeLaPartida estado;

	public Partida(ArrayList<Participante> observadores, ArrayList<PreguntaYRespuesta> preguntasYRespuestas, 
					Map<Participante,ArrayList<PreguntaYRespuesta>> participantes,EstadoDeLaPartida estado) {
		super(observadores);
		this.preguntasYRespuestas = preguntasYRespuestas;
		this.estado = estado;
		this.participantes = participantes;
	}

	public void setEstado(EstadoDeLaPartida estado) {
		this.estado = estado;
	}

	public ArrayList<String> getPreguntas() {
		ArrayList<String> preguntas = new ArrayList<String>();
		this.preguntasYRespuestas.stream().forEach(pYR -> preguntas.add(pYR.getPregunta()));
		return preguntas;
	}
	
	public Map<Participante, ArrayList<PreguntaYRespuesta>> getParticipantes() {
		return participantes;
	}

	public void solicitarUnirse(Participante participante) {
		this.estado.solicitarUnirse(this,participante);
	}
	
	public void finalizarPartida() {
		this.estado.finalizarPartida(this);
	}
	
	public void verificarRespuesta(Participante participante,PreguntaYRespuesta preguntaYRespuesta) {
		this.estado.verificarRespuesta(this, participante, preguntaYRespuesta);
	}
	
	public void agregarParticipante(Participante participante) {
		this.participantes.put(participante, this.preguntasYRespuestas);
	}
	

}
