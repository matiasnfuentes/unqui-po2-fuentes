package ar.edu.unq.po2.tp9.observer.ejercicio3;

public class PreguntaYRespuesta {
	
	private String pregunta;
	private String respuesta;
	
	public PreguntaYRespuesta(String pregunta, String respuesta) {
		this.pregunta = pregunta;
		this.respuesta = respuesta;
	}
	public String getPregunta() {
		return pregunta;
	}
	public String getRespuesta() {
		return respuesta;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (!(obj instanceof PreguntaYRespuesta)) {
			return false;
		}
		PreguntaYRespuesta preguntaYRespuesta = (PreguntaYRespuesta) obj;
		return this.pregunta.equals(preguntaYRespuesta.getPregunta()) 
				&& this.respuesta.equals(preguntaYRespuesta.getRespuesta()) ;
	}

	@Override
	public int hashCode() {
		return this.pregunta.hashCode() + this.respuesta.hashCode();
	}
	
}
