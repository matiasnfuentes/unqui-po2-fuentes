package ar.edu.unq.po2.tp9.observer.ejercicio3;

import java.util.ArrayList;

public interface Participante {
	public String getNombre();
	public void update(String informe);
	public void update(ArrayList<String> preguntas);

}
