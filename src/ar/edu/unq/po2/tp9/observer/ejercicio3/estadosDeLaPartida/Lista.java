package ar.edu.unq.po2.tp9.observer.ejercicio3.estadosDeLaPartida;

import java.util.ArrayList;

import ar.edu.unq.po2.tp9.observer.ejercicio3.Participante;
import ar.edu.unq.po2.tp9.observer.ejercicio3.Partida;
import ar.edu.unq.po2.tp9.observer.ejercicio3.PreguntaYRespuesta;

public class Lista implements EstadoDeLaPartida {

	@Override
	public void verificarRespuesta(Partida partida, Participante participante, PreguntaYRespuesta respuesta) {
		
		ArrayList<PreguntaYRespuesta> preguntasRestantes = partida.getParticipantes().get(participante);
		
		if(preguntasRestantes.contains(respuesta)) {
			if(preguntasRestantes.size()==1) {
				this.finalizarPartida(partida);
				partida.notificarObservadores("La partida ha finaliza, el ganador fue: " + participante.getNombre());
			}
			else {
				preguntasRestantes.remove(respuesta);
				partida.getParticipantes().replace(participante, preguntasRestantes);
				partida.notificarObservador(participante, "Su respuesta es correcta");
				partida.notificarObservadores("El jugador " + participante + " respondio bien a la pregunta : " + respuesta.getPregunta());
			}
		}
		else {
			partida.notificarObservador(participante, "Su respuesta es incorrecta");
		}
	}

	@Override
	public void solicitarUnirse(Partida partida, Participante participante){
		System.out.println("La partida se encuentra llena, no pueden unirse mas jugadores");
	}

	@Override
	public void finalizarPartida(Partida partida) {
		partida.setEstado(new Finalizada());
	}

}
