package ar.edu.unq.po2.tp9.observer.ejercicio3.estadosDeLaPartida;

import ar.edu.unq.po2.tp9.observer.ejercicio3.Participante;
import ar.edu.unq.po2.tp9.observer.ejercicio3.Partida;
import ar.edu.unq.po2.tp9.observer.ejercicio3.PreguntaYRespuesta;

public interface EstadoDeLaPartida {
	public void verificarRespuesta(Partida partida, Participante participante,PreguntaYRespuesta respuesta);
	public void solicitarUnirse(Partida partida, Participante participante);
	public void finalizarPartida(Partida partida);
}
