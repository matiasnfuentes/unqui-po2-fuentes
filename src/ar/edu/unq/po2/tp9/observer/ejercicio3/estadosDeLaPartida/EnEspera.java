package ar.edu.unq.po2.tp9.observer.ejercicio3.estadosDeLaPartida;

import ar.edu.unq.po2.tp9.observer.ejercicio3.Participante;
import ar.edu.unq.po2.tp9.observer.ejercicio3.Partida;
import ar.edu.unq.po2.tp9.observer.ejercicio3.PreguntaYRespuesta;

public class EnEspera implements EstadoDeLaPartida{

	@Override
	public void verificarRespuesta(Partida partida, Participante participante, PreguntaYRespuesta respuesta) {
		System.out.println("La partida todav�a no ha comenzado");
	}

	@Override
	public void solicitarUnirse(Partida partida, Participante participante) {
		
		if(partida.getParticipantes().size()<5) {
			partida.agregarObservador(participante);
			partida.agregarParticipante(participante);
			if(partida.getParticipantes().size()==5) {
				partida.notificarObservadores(partida.getPreguntas());
			}
		}
		
	}

	@Override
	public void finalizarPartida(Partida partida) {
		System.out.println("La partida todav�a no ha comenzado");
	}

}
