package ar.edu.unq.po2.tp9.observer.ejercicio0.complexObserver;


// En esta implementación el sensor envía cierta información acerca del cambio.
// Esto hace que el sujeto tenga algo de conocimiento sobre sus observadores
// Es mas eficiente, pero menos reusable.
// Igualmente son los observadores los que siguen tenienedo que consultar el estado del sensor.
// Es un punto intermedio

import java.util.Observable;
public class ComplexSensor extends Observable{
	
	private int temperaturaExterior;
	private int temperaturaInterior;
	private int humedad;
	
	public ComplexSensor(){
		this.temperaturaExterior=0;
		this.temperaturaInterior=0;
		this.humedad=0;
		this.reset();
	}

	private void reset() {
		// Metodo que simular obtener los valores iniciales del
		//hardware que obtiene las temperaturas y la humedad
		
	}
	
	public void setTemperaturaExterior(int nuevoValor){
		this.temperaturaExterior=nuevoValor;
		this.notificar("TemperaturaExterior");
	}
	
	public void setTemperaturaInterior(int nuevoValor){
		this.temperaturaInterior=nuevoValor;
		this.notificar("TemperaturaInterior");
	}
	
	public void setHumedad(int nuevoValor){
		this.humedad=nuevoValor;
		this.notificar("Humedad");
	}

	public int getTemperaturaExterior() {
		return temperaturaExterior;
	}

	public int getTemperaturaInterior() {
		return temperaturaInterior;
	}

	public int getHumedad() {
		return humedad;
	}
	
	private void notificar(String aspecto){
		this.setChanged();
		this.notifyObservers(aspecto);
	}

	
	
}
