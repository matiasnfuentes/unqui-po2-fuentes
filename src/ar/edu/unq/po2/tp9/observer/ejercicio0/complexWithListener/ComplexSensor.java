package ar.edu.unq.po2.tp9.observer.ejercicio0.complexWithListener;


/*En este caso el sensor notifica a los observadores especificamente que el cambi� (pasandose por parametro), 
 * y que aspecto de su estado cambio. Esto vuelve a los observadores mucho menos reusables, pero a la vez es mas eficiente
 * Ya que los observadores no deben estar preguntando por el estado del sensor.*/
import java.util.ArrayList;
import java.util.List;

public class ComplexSensor {

		
		private int temperaturaExterior;
		private int temperaturaInterior;
		private int humedad;
		private List<SensorListener> listeners;
		
		public ComplexSensor(){
			this.temperaturaExterior=0;
			this.temperaturaInterior=0;
			this.humedad=0;
			this.listeners= new ArrayList<SensorListener>();
			this.reset();
		}

		private void reset() {
			// Metodo que simular obtener los valores iniciales del
			//hardware que obtiene las temperaturas y la humedad
			
		}
		
		public void setTemperaturaExterior(int nuevoValor){
			this.temperaturaExterior=nuevoValor;
			this.notificarTemperaturaExterior();;
		}
		
		private void notificarTemperaturaExterior() {
			for (SensorListener listener : this.listeners) {
				listener.temperaturaExteriorModificada(this, this.getTemperaturaExterior());
			}
		}

		public void setTemperaturaInterior(int nuevoValor){
			this.temperaturaInterior=nuevoValor;
			this.notificarTemperaturaInterior();
		}
		
		private void notificarTemperaturaInterior() {
			for (SensorListener listener : this.listeners) {
				listener.temperaturaInteriorModificada(this, this.getTemperaturaInterior());
			}
		}
		public void setHumedad(int nuevoValor){
			this.humedad=nuevoValor;
			this.notificarHumedad();
		}
		
		private void notificarHumedad() {
			for (SensorListener listener : this.listeners) {
				listener.humedadModificada(this, this.getHumedad());
			}
		}

		public int getTemperaturaExterior() {
			return temperaturaExterior;
		}

		public int getTemperaturaInterior() {
			return temperaturaInterior;
		}

		public int getHumedad() {
			return humedad;
		}
		
}
