package ar.edu.unq.po2.tp9.observer.ejercicio2;

public interface Observador {
	
	public void update(Partido partido);

}
