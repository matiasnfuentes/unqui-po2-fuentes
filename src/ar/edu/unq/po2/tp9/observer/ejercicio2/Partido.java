package ar.edu.unq.po2.tp9.observer.ejercicio2;

import java.util.ArrayList;

public class Partido {
	
	private String resultado;
	private ArrayList<String> contrincantes;
	private String deporte;
	
	
	public Partido(String resultado, ArrayList<String> contrincantes, String deporte) {
		this.resultado = resultado;
		this.contrincantes = contrincantes;
		this.deporte = deporte;
	}
	
	public String getResultado() {
		return resultado;
	}
	public ArrayList<String> getContrincantes() {
		return contrincantes;
	}
	public String getDeporte() {
		return deporte;
	}
	
	

}
