package ar.edu.unq.po2.tp9.observer.ejercicio2;

import java.util.ArrayList;

public class Sujeto {
	
	private ArrayList<ObservadorDeInteres> observadores;

	public Sujeto(ArrayList<ObservadorDeInteres> observadores) {
		this.observadores = observadores;
	}
	
	public void agregarObservador(ObservadorDeInteres observador) {
		this.observadores.add(observador);
	}
	
	public void eliminarObservador(ObservadorDeInteres observador) {
		this.observadores.remove(observador);
	}
	
	public void notificarObservadores(Partido partido) {
		this.observadores.stream().forEach(o -> o.update(partido));
	}
}
