package ar.edu.unq.po2.tp9.observer.ejercicio2;

import java.util.ArrayList;

public class Aplicacion extends Sujeto {

	private ArrayList<Partido> partidos;
	
	public Aplicacion(ArrayList<Partido> partidos,ArrayList<ObservadorDeInteres> observadores) {
		super(observadores);
		this.partidos = partidos;
	}

	public void agregarPartido(Partido partido) {
		this.partidos.add(partido);
	}
}
