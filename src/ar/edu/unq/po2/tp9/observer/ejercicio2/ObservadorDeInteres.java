package ar.edu.unq.po2.tp9.observer.ejercicio2;

public class ObservadorDeInteres {
	
	private Observador observador;
	private Interes interes;

	public ObservadorDeInteres(Observador observador, Interes interes) {
		this.observador = observador;
		this.interes = interes;
	}

	public void update(Partido partido) {
		if(this.chequearInteres(partido)) {
			this.observador.update(partido);
		}
	}

	private boolean chequearInteres(Partido partido) {
		return interes.getDeportes().stream().anyMatch(d -> d.equals(partido.getDeporte())) ||
			   interes.getContrincantes().stream().anyMatch(c -> partido.getContrincantes().contains(c));
	}

}
