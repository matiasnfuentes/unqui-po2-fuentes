package ar.edu.unq.po2.tp9.observer.ejercicio2;

import java.util.ArrayList;

public class Interes {

	private ArrayList<String> deportes;
	private ArrayList<String> contrincantes;
	
	public Interes(ArrayList<String> deportes, ArrayList<String> contrincantes) {
		this.deportes = deportes;
		this.contrincantes = contrincantes;
	}
	public ArrayList<String> getDeportes() {
		return deportes;
	}
	public ArrayList<String> getContrincantes() {
		return contrincantes;
	}
	
	
}
