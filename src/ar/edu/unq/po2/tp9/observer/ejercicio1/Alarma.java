package ar.edu.unq.po2.tp9.observer.ejercicio1;

import java.util.ArrayList;

public class Alarma {
	
	private ArrayList<Suscripcion> suscripciones;

	public Alarma(ArrayList<Suscripcion> suscripciones) {
		this.suscripciones = suscripciones;
	}
	
	public void agregarSuscripcion(Suscripcion suscripcion) {
		this.suscripciones.add(suscripcion);
	}
	
	public void eliminarSuscripcion(Suscripcion suscripcion) {
		this.suscripciones.remove(suscripcion);
	}
	
	public void notificar(Articulo articulo) {
		for (Suscripcion suscripcion : suscripciones) {
			if(suscripcion.compararReferencia(articulo)) {
				suscripcion.actualizar(articulo);
			}
		}
	}

}
