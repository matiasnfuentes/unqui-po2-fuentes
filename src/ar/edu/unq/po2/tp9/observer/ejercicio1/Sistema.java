package ar.edu.unq.po2.tp9.observer.ejercicio1;

import java.util.ArrayList;

public class Sistema {
	
	private ArrayList<Articulo> articulos;
	private Alarma alarma;

	public Sistema(ArrayList<Articulo> articulos, Alarma alarma) {
		this.articulos = articulos;
		this.alarma = alarma;
	}
	
	public void cargarArticulo(Articulo articulo) {
		this.articulos.add(articulo);
		this.alarma.notificar(articulo);
	}

}
