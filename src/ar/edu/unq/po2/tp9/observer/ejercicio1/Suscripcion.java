package ar.edu.unq.po2.tp9.observer.ejercicio1;

public class Suscripcion {
	
	private Observador observador;
	private Referencia referencia;

	public Suscripcion(Observador observador, Referencia referencia) {
		this.observador = observador;
		this.referencia = referencia;
	}

	public Observador getObservador() {
		return observador;
	}

	public boolean compararReferencia(Articulo articulo) {
		return  this.referencia.compararTitulo(articulo.getTitulo()) &&
				this.referencia.compararAutores(articulo.getAutores()) &&
				this.referencia.compararFiliaciones(articulo.getFiliaciones()) &&
				this.referencia.compararLugarDePublicacion(articulo.getLugarDePublicacion()) &&
				this.referencia.compararPalabrasClave(articulo.getPalabrasClave());
	}

	public void actualizar(Articulo articulo) {
		this.observador.actualizar(articulo);
	}

}
