package ar.edu.unq.po2.tp9.observer.ejercicio1;

public interface Observador {

	void actualizar(Articulo articulo);

}
