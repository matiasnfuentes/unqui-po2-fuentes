package ar.edu.unq.po2.tp9.observer.ejercicio1;

import java.util.ArrayList;

public class Articulo {
	
	private String titulo;
	private ArrayList<String> autores;
	private ArrayList<String> filiaciones;
	private String lugarDePublicacion;
	private ArrayList<String> palabrasClave;
	
	
	public Articulo(String titulo, ArrayList<String> autores, ArrayList<String> filiaciones, String lugarDePublicacion,
			ArrayList<String> palabrasClave) {
		this.titulo = titulo;
		this.autores = autores;
		this.filiaciones = filiaciones;
		this.lugarDePublicacion = lugarDePublicacion;
		this.palabrasClave = palabrasClave;
	}
	public String getTitulo() {
		return titulo;
	}
	public ArrayList<String> getAutores() {
		return autores;
	}
	public ArrayList<String> getFiliaciones() {
		return filiaciones;
	}
	public String getLugarDePublicacion() {
		return lugarDePublicacion;
	}
	public ArrayList<String> getPalabrasClave() {
		return palabrasClave;
	}
	
	

}
