package ar.edu.unq.po2.tp9.observer.ejercicio1;

import java.util.ArrayList;

public class Referencia {
	
	private String titulo;
	private ArrayList<String> autores;
	private ArrayList<String> filiaciones;
	private String lugarDePublicacion;
	private ArrayList<String> palabrasClave;
	
	
	public Referencia(String titulo, ArrayList<String> autores, ArrayList<String> filiaciones, String lugarDePublicacion,
					ArrayList<String> palabrasClave) {
		this.titulo = titulo;
		this.autores = autores;
		this.filiaciones = filiaciones;
		this.lugarDePublicacion = lugarDePublicacion;
		this.palabrasClave = palabrasClave;
	}


	public boolean compararTitulo(String titulo) {
		return this.titulo.equals(titulo) || this.titulo.equals("");
	}


	public boolean compararAutores(ArrayList<String> autores) {
		return autores.containsAll(this.autores) || this.autores.isEmpty();
	}


	public boolean compararFiliaciones(ArrayList<String> filiaciones) {
		return filiaciones.containsAll(this.filiaciones) || this.filiaciones.isEmpty();
	}


	public boolean compararLugarDePublicacion(String lugarDePublicacion) {
		return this.lugarDePublicacion.equals(lugarDePublicacion) || this.lugarDePublicacion.equals("");
	}


	public boolean compararPalabrasClave(ArrayList<String> palabrasClave) {
		return palabrasClave.containsAll(this.palabrasClave) || this.palabrasClave.isEmpty();
	}

}
