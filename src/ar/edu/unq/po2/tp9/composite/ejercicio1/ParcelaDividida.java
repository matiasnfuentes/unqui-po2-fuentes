package ar.edu.unq.po2.tp9.composite.ejercicio1;

import java.util.ArrayList;

public class ParcelaDividida extends Parcela {
	
	private ArrayList<Parcela> parcelas;
	
	public ParcelaDividida() {
		this.parcelas = new ArrayList<Parcela>();
	}

	public int subparcelas(){return 4;}
	
	@Override
	public double ganancia() {
		double ganancia = 0;
		for (Parcela parcela : this.getParcelas()) {
			ganancia+= (parcela.ganancia() / parcela.subparcelas());
		}
		return ganancia;
	}
	
	@Override
	public void add(Parcela parcela) {
		if (parcelas.size()<4) {
			parcelas.add(parcela);
		}
	}
	
	@Override
	public void delete(Parcela parcela) {
		parcelas.remove(parcela);
	}
	
	@Override
	public ArrayList<Parcela> getParcelas(){
		return parcelas;
	}

}
