package ar.edu.unq.po2.tp9.composite.ejercicio1;

import java.util.ArrayList;

public abstract class Parcela {
	
	public abstract double ganancia();
	
	public int subparcelas(){return 1;}
	
	public void add(Parcela parcela) throws Exception{
		System.out.println("No puedo dividir esta parcela");
	}
	
	public void delete(Parcela parcela) throws Exception{
		throw new Exception("Esta parcela no esta subdividida");
	}
	
	public ArrayList<Parcela> getParcelas(){
		return null;
	}

}
