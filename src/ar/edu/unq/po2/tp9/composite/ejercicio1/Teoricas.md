2. Describa las alternativas que puede utilizar para calcular la ganancia anual. Relacione las alternativas.

Para calcular la ganancia anual, en mi caso, decidi que cada Parcela sepa en cuantas subparcelas está dividida, 
así las parcelas que no están subdivididas retornaran en valor total de su ganancia , y las que si lo están irán dividiendo su ganancia por 4, dependiendo de las veces que se haya dividido la parcela original.

Otra alternativa podría ser que los Component tengan referencias a sus padres, por lo tanto , si un component es hijo de otro, debe dividir su ganancia por 4.

En este caso creo que la primera solución es mas viable, ya que sino tendría referencias a los padres que en este caso no son necesarias.

3. Explique la discusión sobre quiénes deben implementar las operaciones de agregado y borrado de hijos.

Hay dos corrientes principales en cuanto al borrado y agregado de hijos:
	1. La primera dice que el Component debe ser quién defina estas operaciones, para poder tratar a todos los objetos de la misma forma. Pero de esta manera pierdo seguridad, ya que puede que un cliente trate de agregar un hijo a una hoja
	2. La otra corriente dice que el responsable de definir estas operaciones es la clase Composite, de esta manera gano en seguridad, pero pierdo la versatilidad de manejar a todas las clases de la misma forma

La implementación utilizada va a depender de lo que se busque en el caso particular.

4. Sobre su implementación, indique los roles que desempeñan cada una de las clases diseñadas en relación al patrón Composite.

Parcela -> Component
ParcelaDividida -> Composite
Trigo, Soja -> Leaf