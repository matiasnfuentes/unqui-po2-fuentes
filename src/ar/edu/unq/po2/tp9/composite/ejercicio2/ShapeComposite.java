package ar.edu.unq.po2.tp9.composite.ejercicio2;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;


//Composite 

public class ShapeComposite implements ShapeShifter {
	
	private ArrayList<ShapeShifter> childs;
    
	public ShapeComposite() {
		this.childs = new ArrayList<ShapeShifter>();
	}
	
	public void add(ShapeShifter shape) {
		this.childs.add(shape);
	}

	@Override
	public ShapeShifter compose(ShapeShifter shape) {
		ShapeComposite newShape = new ShapeComposite();
		newShape.add(this);
		newShape.add(shape);
		return newShape;
	}

	@Override
	public int deepest() {
		return childs
					.stream()
					.map(c -> c.deepest())
					.max(Comparator.naturalOrder())
					.get() + 1;
	}

	@Override
	public ShapeShifter flat() {
		List<Integer> shapeValues = this.values();
		ShapeComposite newShape = new ShapeComposite();
		for (Integer value : shapeValues) {
			newShape.add(new ShapeLeaf(value));
		}
		return newShape;
	}

	@Override
	public List<Integer> values() {
		ArrayList<Integer> values = new ArrayList<Integer>();
		for (ShapeShifter shape : childs) {
			values.addAll(shape.values());
		}
		return values;
	}

}
