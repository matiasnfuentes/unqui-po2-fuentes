package ar.edu.unq.po2.tp9.composite.ejercicio2;

public class ShapePruebaInstancia {
	
	public static void main(String[] args) {
		ShapeLeaf a = new ShapeLeaf(1);
		ShapeLeaf b = new ShapeLeaf(5);
		ShapeLeaf e = new ShapeLeaf(8);
		
		// Puedo instanciar un objeto compuesto haciendo composición 
		ShapeShifter c = a.compose(b);
		ShapeShifter d = a.compose(c);
		
		ShapeShifter f = d.compose(e).compose(a);
		
		System.out.println("Deepest a " + a.deepest());
		System.out.println("Values a " + a.values());
		
		System.out.println("Deepest c " + c.deepest());
		System.out.println("Values c " + c.values());
		
		System.out.println("Deepest d " + d.deepest());
		System.out.println("Values d " + d.values());
		
		System.out.println("Deepest f " + f.deepest());
		System.out.println("Values f " + f.values());
		
	}

}
