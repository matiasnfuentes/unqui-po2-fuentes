package ar.edu.unq.po2.tp9.composite.ejercicio2;

import java.util.List;


// Leaf
public class ShapeLeaf implements ShapeShifter {
	
	private int value;

	public ShapeLeaf(int value) {
		this.value = value;
	}

	@Override
	public ShapeShifter compose(ShapeShifter shape) {
		ShapeComposite newShape = new ShapeComposite();
		newShape.add(this);
		newShape.add(shape);
		return newShape;
	}

	@Override
	public int deepest() {
		return 0;
	}

	@Override
	public ShapeShifter flat() {
		return this;
	}

	@Override
	public List<Integer> values() {
		return List.of(this.value);
	}

}
