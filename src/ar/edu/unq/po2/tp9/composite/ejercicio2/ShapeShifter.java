package ar.edu.unq.po2.tp9.composite.ejercicio2;

import java.util.List;

//Component

public interface ShapeShifter {
	
	public ShapeShifter compose(ShapeShifter shape);
	public int deepest(); 
	public ShapeShifter flat(); 
	public List<Integer> values(); 
	
}
