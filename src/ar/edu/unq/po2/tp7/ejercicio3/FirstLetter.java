package ar.edu.unq.po2.tp7.ejercicio3;

public class FirstLetter extends Filter {

	@Override
	public boolean meetCondition(WikipediaPage page, WikipediaPage pageToCompare) {
		return page
					.getTitle()
					.startsWith(pageToCompare
											.getTitle()
											.substring(0, 1));
	}

}
