package ar.edu.unq.po2.tp7.ejercicio3;

import java.util.List;

public class CommonLink extends Filter {

	@Override
	public boolean meetCondition(WikipediaPage page, WikipediaPage pageToCompare) {
		
		List<WikipediaPage> links = page.getLinks();
		List<WikipediaPage> linksToCompare = pageToCompare.getLinks();
		
		return links.stream().anyMatch(l -> linksToCompare.contains(l));
	}

}
