package ar.edu.unq.po2.tp7.ejercicio3;

import java.util.List;
import java.util.stream.Collectors;

public abstract class Filter {
	
	public final List<WikipediaPage> getSimilarPages(WikipediaPage page,List<WikipediaPage> wikipedia){
		return wikipedia
						.stream()
						.filter(p -> this.meetCondition(p,page))
						.collect(Collectors.toList());
	}
	
	public abstract boolean meetCondition(WikipediaPage page,WikipediaPage pageToCompare);

}
