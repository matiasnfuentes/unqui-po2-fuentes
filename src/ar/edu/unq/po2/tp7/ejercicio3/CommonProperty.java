package ar.edu.unq.po2.tp7.ejercicio3;

import java.util.Set;

public class CommonProperty extends Filter {

	@Override
	public boolean meetCondition(WikipediaPage page, WikipediaPage pageToCompare) {
		
		Set<String> propertys = page.getInfobox().keySet();
		Set<String> propertysToCompare = pageToCompare.getInfobox().keySet();
		
		return propertys.stream().anyMatch(p -> propertysToCompare.contains(p));
	}

}
