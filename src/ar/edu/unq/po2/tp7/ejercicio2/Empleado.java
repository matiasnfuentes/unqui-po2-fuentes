package ar.edu.unq.po2.tp7.ejercicio2;

public abstract class Empleado {
	
	// Template method
	public final double sueldo() {
		return (this.sueldoBasico() + this.sueldoPorHoras() + this.adicionales()) * 0.87;
	}
	
	// Hook methods
	public double sueldoBasico(){return 0;}
	public double sueldoPorHoras(){return 0;}
	public double adicionales(){return 0;}
	
}
