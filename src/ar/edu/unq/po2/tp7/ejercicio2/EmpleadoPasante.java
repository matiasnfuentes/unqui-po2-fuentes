package ar.edu.unq.po2.tp7.ejercicio2;

public class EmpleadoPasante extends Empleado {
	
	private int horasTrabajadas;
	
	public EmpleadoPasante(int horasTrabajadas) {
		this.horasTrabajadas = horasTrabajadas;
	}

	// Concrete operations
	
	@Override
	public double sueldoPorHoras(){return 40 * horasTrabajadas;}

}
