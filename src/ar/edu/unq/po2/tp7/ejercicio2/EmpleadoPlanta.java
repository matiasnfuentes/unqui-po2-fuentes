package ar.edu.unq.po2.tp7.ejercicio2;

public class EmpleadoPlanta extends Empleado {
	
	private int cantHijos;
	
	public EmpleadoPlanta(int cantHijos) {
		this.cantHijos = cantHijos;
	}
	
	// Concrete operations

	@Override
	public double sueldoBasico(){return 3000;}
	
	@Override
	public double adicionales(){return cantHijos * 150;}

}
