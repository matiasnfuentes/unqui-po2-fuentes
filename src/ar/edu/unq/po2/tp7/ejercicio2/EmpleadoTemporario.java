package ar.edu.unq.po2.tp7.ejercicio2;

public class EmpleadoTemporario extends Empleado {
	
	private boolean bonusPorFamilia;
	private int horasTrabajadas;
	
	public EmpleadoTemporario(boolean bonusPorFamilia, int horasTrabajadas) {
		this.bonusPorFamilia = bonusPorFamilia;
		this.horasTrabajadas = horasTrabajadas;
	}
	
	// Concrete operations
	
	@Override
	public double sueldoBasico(){return 1000;}
	
	@Override
	public double sueldoPorHoras(){return 5 * horasTrabajadas;}
	
	@Override
	public double adicionales(){
		if (this.bonusPorFamilia) {
			return 100;
		}
		else {
			return 0;
		}
	}
}
