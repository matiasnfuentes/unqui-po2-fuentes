package ar.edu.unq.po2.tp7.ejercicio4;

public abstract class LlamadaTelefonica {
	
	private int tiempo ;
	private int horaDelDia ;
	
	public LlamadaTelefonica( int tiempo , int horaDelDia ){
		this.tiempo = tiempo ;
		this.horaDelDia = horaDelDia ;
	}
	
	// Operacion concreta de la clase abstracta
	public int getTiempo(){
		return this.tiempo;
	}
	
	// Operacion concreta de la clase abstracta
	public int getHoraDelDia(){
		return this.horaDelDia;
	}
	
	// Operacion primitiva
	public abstract boolean esHoraPico();
	
	// Template method
	public float costoFinal(){
		if (this.esHoraPico()){
			return this.costoNeto()*1.2f* this .getTiempo();
		} else {
			return this.costoNeto()* this .getTiempo();
		}
	}
	
	// Hook method
	public float costoNeto(){
		return this .getTiempo() * 1;
	}
	
}
