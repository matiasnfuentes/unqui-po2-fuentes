package ar.edu.unq.po2.tp7.ejercicio4;

public class LlamadaDescuento extends LlamadaTelefonica{
	
	public LlamadaDescuento( int tiempo, int horaDelDia) {
		super (tiempo, horaDelDia);
	}
	
	// Operación concreta 
	@Override
	public boolean esHoraPico() {
		return false ;
	}
	
	// Operación concreta 
	@Override
	public float costoNeto(){
		return this .getTiempo()* 0.95f;
	}
}