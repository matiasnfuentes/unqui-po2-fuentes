package ar.edu.unq.po2.tp7.ejercicio7;

import javax.swing.DefaultListModel;
	
public class DefaultListModelOrdered extends DefaultListModel {
	
	private ListaDePalabrasOrdenadas lista;
	
	public DefaultListModelOrdered() {
		this.lista = new ListaDePalabrasOrdenadas();
	}
	
	@Override
	public void addElement(Object element) {
		lista.agregarPalabra((String) element);
		this.clear();
		for (int i = 0; i < lista.cantidadDePalabras(); i++) {
			this.add(i, lista.getPalabraDePosicion(i));
		}
	}
}
