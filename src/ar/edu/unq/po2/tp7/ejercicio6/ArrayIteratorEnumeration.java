package ar.edu.unq.po2.tp7.ejercicio6;

import java.util.ArrayList;
import java.util.List;

public class ArrayIteratorEnumeration {
	public static void main(String[] args) {
		
		/* Contendra a el Adaptee*/
		ArrayList<Integer> array = new ArrayList<Integer>(List.of(5,6,7));
		
		/*Contendr� una instancia de una clase que implemente iterador, por lo tanto permite utilizar su interfaz
		 * En este caso, ser� el adaptador*/
		IteratorEnumeration iterador = new IteratorEnumeration(array.iterator());
		
		while(iterador.hasMoreElements()){
			System.out.println(iterador.nextElement());
		}
	}


}
