Ejercicio 6 – Contestar y justificar las respuestas

#### 1. Existe más de un tipo de adaptadores, mencione y explique cada uno de ellos.

- **Cuales son los dos acercamientos del patrón adapter:**
1. **Clase adaptadora**  
    - Como funciona?: Esta clase de adaptadores es menos utilizada poque utiliza herencia múltiple. Se crea una clase adaptador que herede de la clase o clases a adaptar, cuyos métodos pueden ser sobreescritos. Además el adaptador deberá implementar la nueva interfaz que deseamos utilizar. La adaptación se realiza llamando a los métodos del padre. No permite la adaptación de las subclases de la clase adaptada.
    - Que características tiene?
        - Adapta el adaptado a el target a través de una clase concreta **"adaptador"**. Como consecuencia la clase adaptador no funcionara  cuando queremos adaptar una clase y todas sus subclases. 
        - Permite al adaptador sobreescribir algunos comportamientos de el adaptado, ya que el Adapter es una subclase del adaptado.
        - Introduce un solo objeto, y ningún puntero adicional de indirección se necesita para obtener el adaptado.

2. **Objeto adaptador**  
    - Como funciona?: Creamos una clase adaptadora que contiene una instancia de la clase o clases a adaptar. Para compatibilizar las clases, el adaptador hace llamadas a la instancia del objeto u objetos a adaptar. Este adaptador funciona con la clase adaptada y todas sus subclases
    - Que características tiene?
        - Permite que un solo adaptador trabaje con muchos adaptados (el adaptado y todas sus subclases). Ademas el adaptador puede añadir funcionalidad a todos los adaptados de una vez.
        - Hace que sea mas difícil sobrescribir el comportamiento del adaptado. Eso requerira subclasear al adaptado y hacer que el adaptador referencie a las subclase, en vez de al adaptador en si mismo.


#### 2. ¿En qué se diferencia un tipo de adaptador del otro?

- La única diferencia es que la clase adaptadora debe heredar de Target y del adaptado (es decir, ser subclase de ambas), mientras que con el objeto adaptador se una la composición para pasar solicitudes a un Adaptado.

#### 3. ¿Se pueden utilizar ambas alternativas de implementación del patrón en java? ¿Justifique la respuesta?

- No, no se pueden utilizar ambos tipos de adaptadores, ya que la clase adaptadora requiere herencia múltiple, y no tenemos disponible esa funcionalidad en Java.

#### 4. Ver la interface Enumeration de java y la clase Vector, preste atención a que dicha clase contiene un método "elements()". Explique cómo funciona el patrón adapter y cuáles son los roles de los participantes entre la interface y clase mencionada. Mencione qué tipo de implementación del patrón se utiliza.

- Funcionamiento:
    - Para este caso la implementación del patrón adapter será a través de un adaptador, que contenga una instancia de la clase vector. La clase adapter implementará los métodos de la interface Enumeration, permitiendo así que cuando se llame al método elements, lo que se devolverá será una instancia de esta clase adaptador, que permita utilizar la interface de Enumeration.
- Roles:
    - La clase vector en este caso será el "Adaptee", la interface Enumeration el "Cliente/target" y habrá una tercera clase que será el "Adapter".

 
#### 5. Realice el mismo análisis del punto 3, pero con la interface Iterator, la clase ArrayList (preste atención al método "iterator()"). Muestre un ejemplo de funcionamiento y especifique los roles de cada participante.

- Funcionamiento:
    - Para este caso la implementación del patrón adapter será a través de un adaptador, que contenga una instancia de la clase ArrayList. La clase adapter implementará los métodos de la interface Iterator, permitiendo así que cuando se llame al método iterator(), lo que se devolverá será una instancia de esta clase adaptador, que permita utilizar la interface de Iterator. La clase adaptador, tendrá que adaptar los metodos de la clase ArrayList, para lograr la funcionalidad deseada de la interface Iterator.
- Roles:
    - La clase ArrayList en este caso será el "Adaptee", la interface Iterator el "Cliente/target" y habrá una tercera clase que será el "Adapter".
- Ejemplo de implementación
~~~import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class ArrayListIterator {
	
    public static void main(String[] args) {
		
        /* Contendra a el Adaptee*/
        ArrayList<Integer> array = new ArrayList<Integer>(List.of(5,6,7));
		
        /*Contendrá una instancia de una clase que implemente iterador, 
        por lo tanto permite utilizar su interfaz. En este caso, será el adaptador*/
        Iterator<Integer> iterador = array.iterator();
		
		/*Imprime cada uno de los elementos del ArrayList a traves del Iterador*/
        while(iterador.hasNext()){
	        System.out.println(iterador.next());
        }
    }
}
~~~