package ar.edu.unq.po2.tp7.ejercicio6;

import java.util.Enumeration;
import java.util.Iterator;

public class IteratorEnumeration implements Enumeration<Object> {
	
	private Iterator<?> iterador;

	public IteratorEnumeration(Iterator<?> iterador) {
		this.iterador = iterador;
	}

	@Override
	public boolean hasMoreElements() {
		return iterador.hasNext();
	}

	@Override
	public Object nextElement() {
		return iterador.next();
	}
	
	public void remove() {
		throw new UnsupportedOperationException();
	}
	
}
