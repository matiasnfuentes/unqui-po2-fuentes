package ar.edu.unq.po2.tp7.ejercicio8;

import org.joda.time.DateTime;

public class FechaAdapter implements IFecha {
	
	private DateTime fecha;

	public FechaAdapter(DateTime fecha) {
		this.fecha = fecha;
	}

	@Override
	public IFecha restarDias(int dias) {
		fecha.minusDays(dias);
		return this;
	}

	@Override
	public boolean antesDeAhora() {
		return fecha.isBeforeNow();
	}

	@Override
	public boolean antesDe(IFecha fecha) {
		return this.fecha.isBefore(new DateTime(fecha.anio(),fecha.mes(),fecha.dia(),0,0));
	}

	@Override
	public boolean despuesDeAhora() {
		return this.fecha.isAfterNow();
	}

	@Override
	public boolean despuesDeFecha(IFecha fecha) {
		return this.fecha.isAfter(new DateTime(fecha.anio(),fecha.mes(),fecha.dia(),0,0));
	}

	@Override
	public int dia() {
		return fecha.getDayOfMonth();
	}

	@Override
	public int mes() {
		return fecha.getMonthOfYear();
	}

	@Override
	public int anio() {
		return fecha.getYear();
	}

}
