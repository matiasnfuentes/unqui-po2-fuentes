package ar.edu.unq.po2.tp4.supermercado;

public class ProductoPrimeraNecesidad extends Producto {
	
	private double descuento;

	public ProductoPrimeraNecesidad(String nombre, double precio, boolean precioCuidado,double descuento) {
		super(nombre, precio, precioCuidado);
		this.descuento = descuento;
	}
	
	public ProductoPrimeraNecesidad(String nombre, double precio, boolean precioCuidado) {
		super(nombre, precio, precioCuidado);
	}

	public ProductoPrimeraNecesidad(String nombre, double precio) {
		super(nombre, precio);
	}
	
	@Override
	public double getPrecio() {
		return super.getPrecio() * this.descuento;
	}

}
