package ar.edu.unq.po2.tp4.trabajador;

import java.util.ArrayList;

public class Trabajador {
	
	private ArrayList<Ingreso> ingresos;
	
	public Trabajador() {
		this.ingresos = new ArrayList<>();
	}
	
	public void agregarIngreso(Ingreso ingreso) {
		this.ingresos.add(ingreso);
	}
	
	public double getTotalPercibido(){
		return ingresos
					.stream()
					.map(ingreso -> ingreso.getMonto())
					.reduce(0.00, (acumulador,item) -> acumulador + item);
	}
	
	public double getMontoImponible(){
		return ingresos
					.stream()
					.map(ingreso -> ingreso.montoImponible())
					.reduce(0.00, (acumulador,item) -> acumulador + item);
	}
	
	public double getImpuestoAPagar() {
		return this.getMontoImponible()* 0.02;
	}

}
