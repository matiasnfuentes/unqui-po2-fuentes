package ar.edu.unq.po2.tp4.trabajador;

public class IngresoPorHorasExtra extends Ingreso {
	
	private int horasExtra;

	public IngresoPorHorasExtra(String mesDePercepcion, String concepto, double monto,int horasExtra) {
		super(mesDePercepcion, concepto, monto);
		this.horasExtra = horasExtra;
	}
	
	@Override
	public double montoImponible() {
		return 0;
	}
}
