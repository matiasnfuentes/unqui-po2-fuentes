package ar.edu.unq.po2.tp2.empresa;

import java.time.LocalDate;

public class scriptCalculoSueldos{
    
    public static void main(String[] args) {
        
        // Instanciacion de la empresa
        // (String nombre, long cuit)
        
        Empresa microsoft = new Empresa("Microsoft",30503285789l);
        
        // Instanciacion de empleado permanente
        // (String nombre, String direccion, EstadoCivil estadoCivil,
        // LocalDate fechaNacimiento,float sueldoBasico,
        // int antig�edad, int hijos )
        
        EmpleadoPermanente mati = new EmpleadoPermanente("Matias",
        "Zeballos 5940",EstadoCivil.Soltero,LocalDate.of(1992,3,24),200,5,0);
        
        // Instanciacion de empleado Temporario
        // ( String nombre, String direccion, EstadoCivil estadoCivil,
        // LocalDate fechaNacimiento,float sueldoBasico, 
        // LocalDate fechaFinDesignación, int horasExtra )
        
        EmpleadoTemporario fede = new EmpleadoTemporario("Federico",
        "Zeballos 5940",EstadoCivil.Soltero,LocalDate.of(1994,4,02),500,
        LocalDate.of(1992,3,24),5);
        
        // Instanciacion de empleado Contratado
        //( String nombre, String direcci�n,EstadoCivil estadoCivil,
        // LocalDate fechaNacimiento,float sueldoBasico,int numeroDeContrato,
        // String medioDePago){
        
        EmpleadoContratado nahue = new EmpleadoContratado("Nahuel","Mitre 200",
        EstadoCivil.Soltero,LocalDate.of(1995,4,02),1000,201,"Efectivo");
        
        // Se agregregan los empleados a la empresa
        
        microsoft.agregarEmpleado(mati);
        microsoft.agregarEmpleado(fede);
        microsoft.agregarEmpleado(nahue);
        
        //Calculos de sueldos brutos y netos
        // Sueldos Brutos : Matias $450, Federico $700, Nahuel $1000
        // Total bruto : $ 2150
        
        // Sueldos Netos : Matias $337.50, Federico $ 535, Nahuel $950
        // Total bruto : $ 1822.50
        
        
        System.out.println("Los sueldos brutos son "+microsoft.sueldosBrutos());
        System.out.println("Los sueldos netos son "+microsoft.sueldosNetos());
        
    }
    
}

