package ar.edu.unq.po2.tp2.empresa;
import java.time.LocalDate;
import java.util.ArrayList;

public class EmpleadoTemporario extends Empleado {
    private final LocalDate fechaFinDesignacion;
    private int horasExtra;
    
    public EmpleadoTemporario( String nombre, String direccion, EstadoCivil estadoCivil,
                               LocalDate fechaNacimiento,float sueldoBasico, 
                               LocalDate fechaFinDesignacion, int horasExtra ){
            
        super(nombre,direccion,estadoCivil,fechaNacimiento,sueldoBasico);
        this.fechaFinDesignacion = fechaFinDesignacion;
        this.horasExtra = horasExtra;
    }
    
    @Override
    public float sueldoBruto(){
        return getSueldoBasico() + horasExtra();
    }
    
    @Override
    protected float obraSocial(){
            return sueldoBruto() * 0.10f + plusEdad(); 
    }
    
    @Override
    protected float aportesJubilatorios(){
            return sueldoBruto() * 0.10f + horasExtra * 5; 
    }
    
    private float plusEdad(){
        if(getEdad()>50){
            return 25;
        }
        else{
            return 0;
        }
    }
    
    private float horasExtra(){
        return horasExtra * 40;
    }

    @Override
    public ArrayList<String> desglosarSueldo() {
        ArrayList<String> desglose = new ArrayList<String>();
        desglose.add("Sueldo Basico       : " + getSueldoBasico());
        desglose.add("Horas Extra         : " + horasExtra());
        desglose.add("Total Bruto         : " + sueldoBruto());
        desglose.add("Retenciones por OS  : " + obraSocial());
        desglose.add("Aportes Jubilatorios: " + aportesJubilatorios());
        desglose.add("Total Retenciones   : " + retenciones());
        desglose.add("Total Neto          : " + sueldoNeto());
        return desglose;
    }

}
