

package ar.edu.unq.po2.tp2.empresa;

import java.time.LocalDate;
import java.util.ArrayList;

/**
 *
 * @author Matias
 */
public class EmpleadoContratado extends Empleado {
    
    private final int numeroDeContrato;
    private String medioDePago;
    
    public EmpleadoContratado( String nombre, String direccion, 
    EstadoCivil estadoCivil,LocalDate fechaNacimiento,float sueldoBasico,
    int numeroDeContrato, String medioDePago){
        
        super(nombre,direccion,estadoCivil,fechaNacimiento,sueldoBasico);
        this.numeroDeContrato = numeroDeContrato;
        this.medioDePago = medioDePago;
    }

    @Override
    public float sueldoBruto() {
        return this.getSueldoBasico();
    }
    
    @Override
    public float retenciones() {
        return 50;
    }

    @Override
    protected float obraSocial() {
        throw new Error("Los empleados contratados no tienen descuentos de OS");
    }

    @Override
    protected float aportesJubilatorios() {
        throw new Error("Los empleados contratados no tienen aportes jubilatorios");
    }

    @Override
    public ArrayList<String> desglosarSueldo() {
        ArrayList<String> desglose = new ArrayList<String>();
        desglose.add("Sueldo Basico           : " + getSueldoBasico());
        desglose.add("Total Bruto             : " + sueldoBruto());
        desglose.add("Total Retenciones       : " + retenciones());
        desglose.add("Total Neto              : " + sueldoNeto());
        return desglose;
    }
    
    
}
