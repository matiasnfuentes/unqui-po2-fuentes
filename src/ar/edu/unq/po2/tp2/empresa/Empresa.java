
package ar.edu.unq.po2.tp2.empresa;

import java.time.LocalDate;
import java.util.ArrayList;



/**
 *
 * @author Matias
 */
public class Empresa {
    private final String nombre;
    private final long cuit;
    private final ArrayList<Empleado> empleados;
    private final ArrayList<Recibo> recibosLiquidados;
    
    public Empresa(String nombre, long cuit){
        this.nombre = nombre;
        this.cuit = cuit;
        this.empleados = new ArrayList<Empleado>();
        this.recibosLiquidados = new ArrayList<Recibo>();
    }
    
    public float sueldosNetos(){
        float total = 0;
        total = empleados.stream().map((empleado) 
                -> empleado.sueldoNeto())
                .reduce(total, (accumulator, _item) -> accumulator + _item);
    return total;
    }
    
    public float sueldosBrutos(){
        float total = 0;
        total = empleados.stream().map((empleado) 
                -> empleado.sueldoBruto())
                .reduce(total, (accumulator, _item) -> accumulator + _item);
    return total;
    }
    
    public float retenciones(){
        float total = 0;
        total = empleados.stream().map((empleado) 
                -> empleado.retenciones())
                .reduce(total, (accumulator, _item) -> accumulator + _item);
    return total;
    }
    
    public void liquidarSueldos(){
        recibosLiquidados.clear();
        for(Empleado empleado: empleados ){
            Recibo reciboSueldo = new Recibo(empleado.getNombre(),
                                        empleado.getDireccion(), 
                                        LocalDate.now(),
                                        empleado.sueldoBruto(), 
                                        empleado.sueldoNeto(),
                                        empleado.desglosarSueldo());
            recibosLiquidados.add(reciboSueldo);
        }
    }
    
    public void agregarEmpleado(Empleado e){
        empleados.add(e);
    }
    
    public void imprimirSueldos(){
        recibosLiquidados.forEach((recibo) -> {recibo.imprimirRecibo();});
    }
    
}
    

