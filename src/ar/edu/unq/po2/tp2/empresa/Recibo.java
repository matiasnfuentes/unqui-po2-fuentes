package ar.edu.unq.po2.tp2.empresa;

import java.time.LocalDate;
import java.util.ArrayList;

class Recibo {
    
    private final String nombreEmpleado;
    private final String direccionEmpleado;
    private final LocalDate fechaEmision;
    private final float sueldoBruto;
    private final float sueldoNeto;
    private ArrayList<String> desglose;

    public Recibo(String nombreEmpleado, String direccionEmpleado, 
                  LocalDate fechaEmision, float sueldoBruto, 
                  float sueldoNeto,ArrayList<String> desglose) {
        
        this.nombreEmpleado = nombreEmpleado;
        this.direccionEmpleado = direccionEmpleado;
        this.fechaEmision = fechaEmision;
        this.sueldoBruto = sueldoBruto;
        this.sueldoNeto = sueldoNeto;
        this.desglose = desglose;
    }
    
    private void mostrarDesglose(){
        desglose.forEach((item) -> {System.out.println(item);});
    }
    
    public void imprimirRecibo(){
        System.out.println("Recibo de sueldos");
        System.out.println();
        System.out.println("Nombre       : " + nombreEmpleado);
        System.out.println("Direcci�n    : " + direccionEmpleado);
        System.out.println("Fecha emisi�n: " + fechaEmision);
        mostrarDesglose();
        System.out.println("-------------------------------------------");
        System.out.println();
    }
    
}
