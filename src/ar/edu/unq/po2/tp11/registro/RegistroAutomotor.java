package ar.edu.unq.po2.tp11.registro;

import java.time.LocalDate;

public class RegistroAutomotor {

	public Boolean debeRealizarVtv(Vehiculo vehiculo, LocalDate fecha) {
		return vehiculo.debeRealizarVtv(fecha);
	}
	
}
