package ar.edu.unq.po2.tp11.registro;

import java.time.LocalDate;

public class VehiculoPolicial extends Vehiculo {

	public VehiculoPolicial(LocalDate fechaFabricacion, String ciudadRadicacion) {
		super(fechaFabricacion, ciudadRadicacion);
	}
	
	@Override
	public Boolean debeRealizarVtv(LocalDate fecha) {
		return false;
	}
	
}
