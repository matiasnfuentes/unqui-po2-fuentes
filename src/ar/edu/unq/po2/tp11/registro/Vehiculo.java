package ar.edu.unq.po2.tp11.registro;

import java.time.LocalDate;

public class Vehiculo {
	
	private LocalDate fechaFabricacion;	
	private String ciudadRadicacion; 
	
	public Vehiculo(LocalDate fechaFabricacion, String ciudadRadicacion) {
		this.fechaFabricacion = fechaFabricacion;
		this.ciudadRadicacion = ciudadRadicacion;
	}

	public LocalDate getFechaFabricacion() {
		return fechaFabricacion;
	}

	public String ciudadRadicacion() {
		return ciudadRadicacion;
	}
	
	public Boolean debeRealizarVtv(LocalDate fecha) {

		return (fecha.minusYears(1).isAfter(fechaFabricacion)
				&& ciudadRadicacion.equalsIgnoreCase("Buenos Aires"));

	}
}
