package ar.edu.unq.po2.tp5.laCajaMercadoCentral;

import java.util.ArrayList;

public class Caja {
	
	public double cobrarConceptos(ArrayList<Cobrable> cobrables) {
		double montoTotal = 0;
		for(Cobrable cobrable : cobrables) {
			montoTotal = montoTotal + cobrable.registrar();
		}
		return montoTotal;
	}

}
