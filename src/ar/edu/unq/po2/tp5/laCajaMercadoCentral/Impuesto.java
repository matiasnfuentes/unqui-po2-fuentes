package ar.edu.unq.po2.tp5.laCajaMercadoCentral;

public class Impuesto extends Factura {
	
	private double tasaDeServicio;
	
	public Impuesto(Agencia agencia,double tasaDeServicio) {
		super(agencia);
		this.tasaDeServicio = tasaDeServicio;
	}

	public double montoAPagar() {
		return this.tasaDeServicio;
	}

}
