package ar.edu.unq.po2.tp5.laCajaMercadoCentral;

public class Producto implements Cobrable {
	private int stock;
	private double precio;
	
	public Producto(int stock, double precio) {
		this.stock = stock;
		this.precio = precio;
	}
	
	public double registrar() {
		this.stock-=1;
		return this.getPrecio();
	}
	
	public double getPrecio() {
		return this.precio;
	}
	
	public int getStock() {
		return this.stock;
	}
	
	
}
