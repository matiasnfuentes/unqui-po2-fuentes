package ar.edu.unq.po2.tp5.laCajaMercadoCentral;


public class ProductoCooperativa extends Producto {

	public ProductoCooperativa(int stock, double precio) {
		super(stock, precio);
	}
	
	@Override
	public double getPrecio() {
		return super.getPrecio()* 0.90;
	}

}
