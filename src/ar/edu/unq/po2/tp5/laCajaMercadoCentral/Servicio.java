package ar.edu.unq.po2.tp5.laCajaMercadoCentral;

public class Servicio extends Factura {
	
	double costoUConsumida;
	int cantUConsumidas;
	
	public Servicio(Agencia agencia,double costoUConsumida, int cantUConsumidas) {
		super(agencia);
		this.costoUConsumida = costoUConsumida;
		this.cantUConsumidas = cantUConsumidas;
	}
	
	public double montoAPagar() {
		return this.costoUConsumida * this.cantUConsumidas;
	}

}
