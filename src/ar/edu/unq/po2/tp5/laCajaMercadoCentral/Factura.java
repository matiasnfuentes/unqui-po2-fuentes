	package ar.edu.unq.po2.tp5.laCajaMercadoCentral;

public abstract class Factura implements Cobrable{

	private Agencia agenciaControladora;
	
	public Factura(Agencia agenciaControladora) {
		this.agenciaControladora = agenciaControladora;
	}
	
	public double registrar() {
		agenciaControladora.registrarPago(this);
		return this.montoAPagar();
	}
	
	public abstract double montoAPagar();

}
