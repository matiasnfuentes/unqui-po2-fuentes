package ar.edu.unq.po2.tp6.bancoYPrestamos;

public abstract class Solicitud {
	
	protected Cliente cliente;
	protected double monto;
	protected int plazo;
	
	public double cuotaMensual() {
		return this.monto / this.plazo;
	}
	
	public double getMonto() {
		return this.monto;
	}
	
	public abstract boolean chequearSolicitud();

}
