package ar.edu.unq.po2.tp6.bancoYPrestamos;

import java.util.ArrayList;

public class CalculoCrediticio {
	
	private static CalculoCrediticio _instance;
	// private static CalculoCrediticio _instance = new CalculoCrediticio();
	
	private CalculoCrediticio() {
	}
	
	public static CalculoCrediticio getInstance() {
		if(_instance==null) {
			_instance = new CalculoCrediticio();
		}
		return _instance;
	}

	public double montoADesembolsar(ArrayList<Solicitud> solicitudes) {
		return solicitudes
						.stream()
						.filter(solicitud -> solicitud.chequearSolicitud())
						.map(s -> s.getMonto())
						.reduce(0.00, (total, monto) -> total + monto);
	}

}
