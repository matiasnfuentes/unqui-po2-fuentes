package ar.edu.unq.po2.tp6.bancoYPrestamos;

public class SolicitudCreditoPersonal extends Solicitud {
	
	public SolicitudCreditoPersonal(Cliente cliente, double monto, int plazo) {
		this.cliente = cliente;
		this.monto = monto;
		this.plazo = plazo;
	}

	@Override
	public boolean chequearSolicitud() {
		return (this.cliente.sueldoNetoAnual()>= 15000) &&
			   (this.cuotaMensual()<= this.cliente.getSueldoNeto() * 0.70);
	}

}
