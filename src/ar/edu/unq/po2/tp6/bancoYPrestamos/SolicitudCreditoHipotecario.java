package ar.edu.unq.po2.tp6.bancoYPrestamos;

import java.time.LocalDate;
import java.time.Period;

public class SolicitudCreditoHipotecario extends Solicitud {
	
	private Propiedad garantia;
	
	public SolicitudCreditoHipotecario(Cliente cliente, double monto, int plazo,Propiedad garantia) {
		this.cliente = cliente;
		this.monto = monto;
		this.plazo = plazo;
		this.garantia = garantia;
	}

	@Override
	public boolean chequearSolicitud() {
		return (this.cuotaMensual()<= this.cliente.getSueldoNeto() * 0.50) &&
			   (this.getMonto()<= this.garantia.gerValorFiscal() * 0.70) &&
			   (this.edadCorrectaParaSolicitud());
	}
	
	private boolean edadCorrectaParaSolicitud() {
		return Period.between(this.cliente.getFechaNacimiento(),
			   LocalDate.now().plusMonths(this.plazo)).getYears() <= 65;
	}
	
}
