package ar.edu.unq.po2.tp6.bancoYPrestamos;

import java.util.ArrayList;

public class Banco {
	
	private ArrayList<Solicitud> solicitudes;
	private ArrayList<Cliente> clientes;
	
	
	public Banco() {
		this.solicitudes = new ArrayList<Solicitud>();
		this.clientes = new ArrayList<Cliente>();
	}

	public void agregarCliente(Cliente cliente) {
		this.clientes.add(cliente);
	}
	
	public void registrarSolicitud(Solicitud solicitud) {
		this.solicitudes.add(solicitud);
	}
	
	public double montoADesembolsar() {
		return CalculoCrediticio.getInstance().montoADesembolsar(this.solicitudes);
	}

}
