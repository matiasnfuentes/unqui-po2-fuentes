package ar.edu.unq.po2.tp6.bancoYPrestamos;

import java.time.LocalDate;
import java.time.Period;

public class Cliente {
	
	private String apellido;
	private String nombre;
	private String direccion;
	private LocalDate fechaNacimiento;
	private double sueldoNeto;
	
	public Cliente(String apellido, String nombre, String direccion, LocalDate fechaNacimiento, double sueldoNeto) {
		super();
		this.apellido = apellido;
		this.nombre = nombre;
		this.direccion = direccion;
		this.fechaNacimiento = fechaNacimiento;
		this.sueldoNeto = sueldoNeto;
	}

	public int getEdad(){
        return (Period.between(this.fechaNacimiento, LocalDate.now()).getYears());    
	}
	
	public double getSueldoNeto() {
		return this.sueldoNeto;
	}
	
	public LocalDate getFechaNacimiento() {
		return this.fechaNacimiento;
	}
	
	public double sueldoNetoAnual() {
		return this.sueldoNeto * 12 ;
	}
	
	public void pedirCreditoHipotecario(Banco banco,double monto, int cuotas,Propiedad garantia) {
		banco.registrarSolicitud(new SolicitudCreditoHipotecario(this,monto,cuotas,garantia));
	}
	public void pedirCreditoPersonal(Banco banco,double monto, int cuotas) {
		banco.registrarSolicitud(new SolicitudCreditoPersonal(this,monto,cuotas));
	}

}
