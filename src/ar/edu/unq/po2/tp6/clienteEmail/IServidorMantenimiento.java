package ar.edu.unq.po2.tp6.clienteEmail;

public interface IServidorMantenimiento {
	
	public float tazaDeTransferencia();

	public void resetear();
	
	public void realizarBackUp();

}
