package ar.edu.unq.po2.tp6.clienteEmail;

import java.util.ArrayList;

public class BandejaEntrada {
	
	private ArrayList<Correo> inbox;
	
	public BandejaEntrada() {
		this.inbox = new ArrayList<Correo>();
	}
	
	public int contarInbox(){
		return this.inbox.size();
	}
	
	public void borrarCorreo(Correo correo){
		this.inbox.remove(correo);
	}
}
