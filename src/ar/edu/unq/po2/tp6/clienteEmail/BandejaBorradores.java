package ar.edu.unq.po2.tp6.clienteEmail;

import java.util.ArrayList;

public class BandejaBorradores {
	
	private ArrayList<Correo> borrados;
	
	public int contarBorrados(){
		return this.borrados.size();
	}
	
	public void eliminarBorrado(Correo correo){
		this.borrados.remove(correo);
	}
	
	public void agregarBorrado(Correo correo) {
		this.borrados.add(correo);
	}

}
