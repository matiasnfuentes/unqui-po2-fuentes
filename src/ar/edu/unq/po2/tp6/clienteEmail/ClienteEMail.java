package ar.edu.unq.po2.tp6.clienteEmail;

public class ClienteEMail {
	
	 private IServidorConectividad servidor;
	 private Usuario usuario;
	 private BandejaEntrada inbox;
	 private BandejaBorradores borrados;
	
	public ClienteEMail(IServidorConectividad servidor, String nombreUsuario, String pass){
		this.servidor=servidor;
		this.usuario = new Usuario(nombreUsuario,pass);
		this.inbox = new BandejaEntrada();
		this.borrados = new BandejaBorradores();
		this.conectar();
	}
	
	public void conectar(){
		this.servidor.conectar(this.usuario.getNombreUsuario(),this.usuario.getPassUsuario());
	}
	
	public void borrarCorreo(Correo correo){
		this.inbox.borrarCorreo(correo);
		this.borrados.agregarBorrado(correo);
	}
	
	public int contarBorrados(){
		return this.borrados.contarBorrados();
	}
	
	public int contarInbox(){
		return this.inbox.contarInbox();
	}
	
	public void eliminarBorrado(Correo correo){
		this.borrados.eliminarBorrado(correo);
	}
	
	public void recibirNuevos(){
		this.servidor.recibirNuevos(this.usuario.getNombreUsuario(),this.usuario.getPassUsuario());
	}
	
	public void enviarCorreo(String asunto, String destinatario, String cuerpo){
		Correo correo = new Correo(asunto, destinatario, cuerpo);
		this.servidor.enviar(correo);
	}

}
