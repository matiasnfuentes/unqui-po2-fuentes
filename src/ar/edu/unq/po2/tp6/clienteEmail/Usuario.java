package ar.edu.unq.po2.tp6.clienteEmail;

public class Usuario {
	
	private String nombreUsuario;
	private String passUsuario;
	
	public Usuario(String nombreUsuario, String passusuario) {
		this.nombreUsuario = nombreUsuario;
		this.passUsuario = passusuario;
	}

	public String getNombreUsuario() {
		return nombreUsuario;
	}

	public String getPassUsuario() {
		return passUsuario;
	}

}
