package ar.edu.unq.po2.tp10.ejercicio3;

public class PlayingSong extends State {
	
	public void pause(Mp3Player player,Song song) throws Exception {
		song.pause();
		player.setState(new Paused());
	}
	
	@Override
	public void stop(Mp3Player player,Song song){
		song.stop();
		player.setState(new SongSelection());
	}

}
