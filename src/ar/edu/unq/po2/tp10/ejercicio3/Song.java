package ar.edu.unq.po2.tp10.ejercicio3;

public interface Song {
	
	public void play();
	public void pause();
	public void stop();

}
