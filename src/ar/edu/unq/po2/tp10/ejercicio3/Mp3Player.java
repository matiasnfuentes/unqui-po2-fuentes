package ar.edu.unq.po2.tp10.ejercicio3;

public class Mp3Player {
	
	private State state;
	private Song actualSong;
	
	public void setState(State state) {
		this.state = state;
	}

	public void setActualSong(Song actualSong) {
		this.actualSong = actualSong;
	}

	public void play(Song song) throws Exception {
		state.play(this,song);
	}

	public void pause() throws Exception{
		state.pause(this,this.actualSong);
	}
	
	public void stop() {
		state.stop(this,this.actualSong);
	}
	
	
}
