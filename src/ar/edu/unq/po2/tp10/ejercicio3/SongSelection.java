package ar.edu.unq.po2.tp10.ejercicio3;

public class SongSelection extends State {
	
	@Override
	public void play(Mp3Player player, Song song) throws Exception {
		song.play();
		player.setActualSong(song);
		player.setState(new PlayingSong());
	}
	
}
