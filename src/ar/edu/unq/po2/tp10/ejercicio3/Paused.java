package ar.edu.unq.po2.tp10.ejercicio3;

public class Paused extends State {

	@Override
	public void pause(Mp3Player player, Song song) throws Exception {
		song.play();
		player.setState(new PlayingSong());
	}

}
