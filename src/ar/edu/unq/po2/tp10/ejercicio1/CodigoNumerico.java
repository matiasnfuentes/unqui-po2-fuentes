package ar.edu.unq.po2.tp10.ejercicio1;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class CodigoNumerico implements ModoDeEncriptacion {

	@Override
	public String encriptar(String texto) {
		String textoEncriptado = "";
		textoEncriptado = encriptarCaracter(texto.charAt(0));
		for (int n = 1; n <texto.length (); n ++) {
			textoEncriptado = textoEncriptado +"," +  this.encriptarCaracter(texto.charAt(n));
		}
		return textoEncriptado;
	}

	private String encriptarCaracter(char c) {
		Integer caracterEncriptado;
		if (Character.isSpaceChar(Character.toLowerCase(c))) {
			caracterEncriptado = 0;
		}
		else {
			caracterEncriptado = Character.getNumericValue(Character.toLowerCase(c)) - 9; // Se le resta 9 para aprovechar la funcion getNumericValue
		}																				  // y obtener el codigo deseado
		return caracterEncriptado.toString();	
	}

	@Override
	public String desencriptar(String texto) {
		String textoDesencriptado = "";
		List<String> codigoEnStrings = Arrays.asList(texto.split(","));
		List<Integer> codigo = codigoEnStrings.stream().map( s -> Integer.valueOf(s)).collect(Collectors.toList());
		textoDesencriptado = codigo.stream().map(c -> this.desencriptarCaracter(c)).collect(Collectors.joining());
		return textoDesencriptado;
	}
	
	private String desencriptarCaracter(int c) {
		Character caracterDesencriptado;
		if (c==0) {
			caracterDesencriptado =' ';
		}
		else {
			caracterDesencriptado = (char) (c + 96); // Se le suma 96 para aprovechar el codigo ascii y obtener la letra deseada
		}
		return caracterDesencriptado.toString();
	}

}
