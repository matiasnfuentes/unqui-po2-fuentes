package ar.edu.unq.po2.tp10.ejercicio1;

import java.util.HashMap;
import java.util.Map;

public class CambiarVocal implements ModoDeEncriptacion {

	private Map<Character,Character> codigoDeEncriptacion;
	private Map<Character,Character> codigoDeDesencriptacion;
	
	
	public CambiarVocal() {
		this.codigoDeEncriptacion = new HashMap<Character,Character>();
		this.codigoDeEncriptacion.put('a', 'e');
		this.codigoDeEncriptacion.put('e', 'i');
		this.codigoDeEncriptacion.put('i', 'o');
		this.codigoDeEncriptacion.put('o', 'u');
		this.codigoDeEncriptacion.put('u', 'a');
		
		this.codigoDeDesencriptacion = new HashMap<Character,Character>();
		this.codigoDeDesencriptacion.put('a', 'u');
		this.codigoDeDesencriptacion.put('e', 'a');
		this.codigoDeDesencriptacion.put('i', 'e');
		this.codigoDeDesencriptacion.put('o', 'i');
		this.codigoDeDesencriptacion.put('u', 'o');
	}
	
	public void cambiarCodigoDeEncriptacion(Map<Character,Character> nuevoCodigo) {
		this.codigoDeEncriptacion = nuevoCodigo;
	}
	
	public void cambiarCodigoDeDesencriptacion(Map<Character,Character> nuevoCodigo) {
		this.codigoDeDesencriptacion = nuevoCodigo;
	}

	@Override 
	public String encriptar(String texto) {
		String textoEncriptado = "";
		for (int n = 0; n <texto.length (); n ++) {
			textoEncriptado = textoEncriptado +  this.encriptarCaracter(texto.charAt(n));
		}
		return textoEncriptado;
	}

	@Override
	public String desencriptar(String texto) {
		String textoDesencriptado = "";
		for (int n = 0; n <texto.length (); n ++) {
			textoDesencriptado  = textoDesencriptado  +  this.desencriptarCaracter(texto.charAt(n));
		}
		return textoDesencriptado ;
	}
	
	private char encriptarCaracter(char c) {
		char caracterEncriptado;
		if(this.codigoDeEncriptacion.containsValue(c)) {
			caracterEncriptado = this.codigoDeEncriptacion.get(c);
		}
		else {
			caracterEncriptado = c;
		}
		return caracterEncriptado;
	}
	
	private char desencriptarCaracter(char c) {
		char caracterDesencriptado;
		if(this.codigoDeDesencriptacion.containsValue(c)) {
			caracterDesencriptado = this.codigoDeDesencriptacion.get(c);
		}
		else {
			caracterDesencriptado = c;
		}
		return caracterDesencriptado;
	}

}
