package ar.edu.unq.po2.tp10.ejercicio1;

public class EncriptadorNaive {
	
	private ModoDeEncriptacion modo;
	
	public EncriptadorNaive(ModoDeEncriptacion modo) {
		this.modo = modo;
	}
	
	public String encriptar(String texto) {
		return modo.encriptar(texto);
	}
	
	public String desencriptar(String texto) {
		return modo.desencriptar(texto);
	}
	
	public void cambiarModoDeEncriptacion(ModoDeEncriptacion modo) {
		this.modo = modo;
	}

}
