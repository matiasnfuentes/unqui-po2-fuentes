package ar.edu.unq.po2.tp10.ejercicio1;

public interface ModoDeEncriptacion {

	public String encriptar(String texto);
	public String desencriptar(String texto);
	
}
