package ar.edu.unq.po2.tp10.ejercicio2;

public class ModoDosJugadores extends Estado {
	
	@Override
	public void finalizar(VideoJuego juego) {
		juego.setEstadoDelJuego(new Encendido());
	}

}
