package ar.edu.unq.po2.tp10.ejercicio2;

public class Encendido extends Estado {
	
	public void empezar(VideoJuego juego,int monedas) {
		if(monedas==1) {
			juego.setEstadoDelJuego(new ModoUnJugador());
		}
		else {
			juego.setEstadoDelJuego(new ModoDosJugadores());
		}
	}

}
