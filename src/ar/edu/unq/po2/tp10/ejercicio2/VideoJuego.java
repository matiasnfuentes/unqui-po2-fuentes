package ar.edu.unq.po2.tp10.ejercicio2;

public class VideoJuego {
	
	private Estado estadoDelJuego;

	public VideoJuego(Estado estadoDelJuego) {
		this.estadoDelJuego = estadoDelJuego;
	}
	
	public void setEstadoDelJuego(Estado estadoDelJuego) {
		this.estadoDelJuego = estadoDelJuego;
	}

	public void emepezar() {
		estadoDelJuego.empezar(this);
	}
	public void empezar(int monedas) {
		if(monedas>=1) {
			estadoDelJuego.empezar(this,monedas);	
		}	
	}
	
	public void finalizar() {
		estadoDelJuego.finalizar(this);
	}

}
