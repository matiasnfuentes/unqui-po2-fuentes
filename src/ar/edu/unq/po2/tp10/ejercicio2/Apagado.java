package ar.edu.unq.po2.tp10.ejercicio2;

public class Apagado extends Estado {
	
	@Override
	public void empezar(VideoJuego juego) {
		juego.setEstadoDelJuego(new Encendido());
	}
	
	@Override
	public void empezar(VideoJuego juego, int monedas) {
		juego.setEstadoDelJuego(new Encendido());
	}

}
