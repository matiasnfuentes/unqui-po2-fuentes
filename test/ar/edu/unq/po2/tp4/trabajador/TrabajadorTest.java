package ar.edu.unq.po2.tp4.trabajador;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import ar.edu.unq.po2.tp4.trabajador.Ingreso;
import ar.edu.unq.po2.tp4.trabajador.IngresoPorHorasExtra;
import ar.edu.unq.po2.tp4.trabajador.Trabajador;


class TrabajadorTest {
	
		private Trabajador jorge;
		private Ingreso ingreso1;
		private IngresoPorHorasExtra ingreso2;
		private Ingreso ingreso3;
		private IngresoPorHorasExtra ingreso4;
		private IngresoPorHorasExtra ingreso5;
		
	
	@BeforeEach
	public void setUp() {
		jorge = new Trabajador();
		ingreso1 = new Ingreso("Marzo","Sueldo Basico",400.00);
		ingreso2 = new IngresoPorHorasExtra("Marzo","Horas Extra Marzo",400.00,5);
		ingreso3 = new Ingreso("Abril","Sueldo Basico",400.00);
		ingreso4 = new IngresoPorHorasExtra("Abril","Horas Extra Abril",1000.00,5);
		ingreso5 = new IngresoPorHorasExtra("Mayo","Horas Extra Mayo",1000.00,5);
		jorge.agregarIngreso(ingreso1);
		jorge.agregarIngreso(ingreso2);
		jorge.agregarIngreso(ingreso3);
		jorge.agregarIngreso(ingreso4);
		jorge.agregarIngreso(ingreso5);
	}

	@Test
	void testTotalPercibido() {
		assertEquals(3200, jorge.getTotalPercibido());
	}
	
	@Test
	void testMontoImponible() {
		assertEquals(800, jorge.getMontoImponible());
	}
	
	@Test
	void testImpuesto() {
		assertEquals(16, jorge.getImpuestoAPagar());
	}
	
	

}
