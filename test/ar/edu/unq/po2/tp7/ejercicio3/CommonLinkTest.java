package ar.edu.unq.po2.tp7.ejercicio3;

import static org.mockito.Mockito.*;

import java.util.List;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;

class CommonLinkTest {

	WikipediaPage page = mock(WikipediaPage.class);
	WikipediaPage pageToCompare = mock(WikipediaPage.class);
	
	WikipediaPage link1 = mock(WikipediaPage.class);
	WikipediaPage link2 = mock(WikipediaPage.class);
	WikipediaPage link3 = mock(WikipediaPage.class);
	
	CommonLink filter = new CommonLink();

	@Test
	void testFiltroExitoso() {
		when(page.getLinks()).thenReturn(List.of(link1,link2));
		when(pageToCompare.getLinks()).thenReturn(List.of(link2,link3));
		assertTrue(filter.meetCondition(page, pageToCompare));
	}
	
	@Test
	void testFiltroFallido() {
		when(page.getLinks()).thenReturn(List.of(link1,link2));
		when(pageToCompare.getLinks()).thenReturn(List.of(link3));
		assertFalse(filter.meetCondition(page, pageToCompare));
	}

}
