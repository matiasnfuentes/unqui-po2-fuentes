package ar.edu.unq.po2.tp7.ejercicio3;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;
import static org.mockito.Mockito.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

class CommonPropertyTest {


	WikipediaPage page = mock(WikipediaPage.class);
	WikipediaPage pageToCompare = mock(WikipediaPage.class);
	
	WikipediaPage link1 = mock(WikipediaPage.class);
	WikipediaPage link2 = mock(WikipediaPage.class);
	WikipediaPage link3 = mock(WikipediaPage.class);
	
	CommonProperty filter = new CommonProperty();

	@Test
	void testFiltroExitoso() {
		//Set up
		Map<String, WikipediaPage> map = new HashMap<String, WikipediaPage>();
		map.put("propiedad1", link1);
		map.put("propiedad2", link1);
		
		Map<String, WikipediaPage> mapToCompare = new HashMap<String, WikipediaPage>();
		mapToCompare.put("propiedad1", link2);
		mapToCompare.put("propiedad2", link3);
		mapToCompare.put("propiedad3", link3);
		
		when(page.getInfobox()).thenReturn(map);
		when(pageToCompare.getInfobox()).thenReturn(mapToCompare);
		
		//Verify
		assertTrue(filter.meetCondition(page, pageToCompare));
	}
	
	@Test
	void testFiltroFallido() {
		//Set up
		Map<String, WikipediaPage> map = new HashMap<String, WikipediaPage>();
		map.put("propiedad1", link1);
		map.put("propiedad2", link1);
		
		Map<String, WikipediaPage> mapToCompare = new HashMap<String, WikipediaPage>();
		mapToCompare.put("propiedad3", link1);
		mapToCompare.put("propiedad4", link1);
		mapToCompare.put("propiedad5", link1);
		
		when(page.getInfobox()).thenReturn(map);
		when(pageToCompare.getInfobox()).thenReturn(mapToCompare);
		
		//Verify
		assertFalse(filter.meetCondition(page, pageToCompare));
	}

}
