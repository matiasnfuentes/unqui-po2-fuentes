package ar.edu.unq.po2.tp7.ejercicio3;

import static org.mockito.Mockito.*;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;

class FirstLetterTest {

	WikipediaPage page = mock(WikipediaPage.class);
	WikipediaPage pageToCompare = mock(WikipediaPage.class);
	FirstLetter filter = new FirstLetter();

	@Test
	void testFiltroExitoso() {
		when(page.getTitle()).thenReturn("Deportes");
		when(pageToCompare.getTitle()).thenReturn("Diario");
		assertTrue(filter.meetCondition(page, pageToCompare));
	}
	
	@Test
	void testFiltroFallido() {
		when(page.getTitle()).thenReturn("Deportes");
		when(pageToCompare.getTitle()).thenReturn("La luna");
		assertFalse(filter.meetCondition(page, pageToCompare));
	}

	
}
