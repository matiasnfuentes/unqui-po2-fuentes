package ar.edu.unq.po2.tp10.ejercicio2;

import static org.mockito.Mockito.*;
import org.junit.jupiter.api.Test;

class ModoDosJugadoresTest {

	VideoJuego juego = mock(VideoJuego.class);
	ModoDosJugadores dosJugadores = new ModoDosJugadores();
	
	// El juego ya esta empezado, no recibe ningun mensaje ante esta interaccion
	@Test
	void testEmpezarJuego() {
		dosJugadores.empezar(juego);
		verifyNoInteractions(juego);
	}
	
	// El juego ya esta empezado, no recibe ningun mensaje ante esta interaccion
	@Test
	void testEmpezarJuegoConMonedas() {
		dosJugadores.empezar(juego,4);
		verifyNoInteractions(juego);
	}
	
	// El juego finaliza porque ya esta en una partida.
	@Test
	void testFinalizarJuego() {
		dosJugadores.finalizar(juego);
		verify(juego).setEstadoDelJuego(any(Encendido.class));
	}

}
