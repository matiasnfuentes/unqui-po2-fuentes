package ar.edu.unq.po2.tp10.ejercicio2;

import static org.mockito.Mockito.*;
import org.junit.jupiter.api.Test;

class EncendidoTest {

	VideoJuego juego = mock(VideoJuego.class);
	Encendido encendido = new Encendido();
	
	// El juego no empieza porque no se insertaron monedas
	@Test
	void testEmpezarJuego() {
		encendido.empezar(juego);
		verifyNoInteractions(juego);
	}
	
	// Test el juego empieza con 1 jugador, porque se inserto una moneda
	@Test
	void testEmpezarJuegoConMonedas() {
		encendido.empezar(juego,1);
		verify(juego).setEstadoDelJuego(any(ModoUnJugador.class));
	}
	// Test el juego empieza con 2 jugadores, porque se insertaron dos monedas
	@Test
	void testEmpezarJuegoConMonedasDosJugadores() {
		encendido.empezar(juego,2);
		verify(juego).setEstadoDelJuego(any(ModoDosJugadores.class));
	}
	
	// Test el juego no finaliza porque esta en modo encendido.
	@Test
	void testFinalizarJuego() {
		encendido.finalizar(juego);
		verifyNoInteractions(juego);
	}

}
