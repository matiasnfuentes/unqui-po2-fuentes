package ar.edu.unq.po2.tp10.ejercicio2;

import static org.mockito.Mockito.*;
import org.junit.jupiter.api.Test;

class ApagadoTest {

	VideoJuego juego = mock(VideoJuego.class);
	Apagado apagado = new Apagado();
	
	// Test empezar el juego
	@Test
	void testEmpezarJuego() {
		apagado.empezar(juego);
		verify(juego).setEstadoDelJuego(any(Encendido.class));
	}
	
	// Test empezar el juego aunque se hayan insertado monedas
	@Test
	void testEmpezarJuegoConMonedas() {
		apagado.empezar(juego,4);
		verify(juego).setEstadoDelJuego(any(Encendido.class));
	}
	
	// El juego no finaliza porque esta apagado.
	@Test
	void testFinalizarJuego() {
		apagado.finalizar(juego);
		verifyNoInteractions(juego);
	}

}
