package ar.edu.unq.po2.tp10.ejercicio2;


import static org.mockito.Mockito.*;
import org.junit.jupiter.api.Test;

class ModoUnJugadorTest {


	VideoJuego juego = mock(VideoJuego.class);
	ModoUnJugador unJugador = new ModoUnJugador();
	
	// El juego ya esta empezado, no recibe ningun mensaje ante esta interaccion
	@Test
	void testEmpezarJuego() {
		unJugador.empezar(juego);
		verifyNoInteractions(juego);
	}
	
	// El juego ya esta empezado, no recibe ningun mensaje ante esta interaccion
	@Test
	void testEmpezarJuegoConMonedas() {
		unJugador.empezar(juego,4);
		verifyNoInteractions(juego);
	}
	
	// El juego finaliza porque ya esta en una partida.
	@Test
	void testFinalizarJuego() {
		unJugador.finalizar(juego);
		verify(juego).setEstadoDelJuego(any(Encendido.class));
	}

}
