package ar.edu.unq.po2.tp10.ejercicio3;

import static org.junit.Assert.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;
import org.junit.jupiter.api.Test;

class PausedTest {

	Song song = mock(Song.class);
	Mp3Player player = mock(Mp3Player.class);
	Paused paused = new Paused();
	
	// Test play
	@Test
	void testPlay() throws Exception {
		assertThrows(Exception.class, () -> { paused.play(player, song); });
	}
	
	// Test stop
	@Test
	void testStop() {
		paused.stop(player, song);
		verifyNoInteractions(player);
	}
	
	// Test pause
	@Test
	void testPause() throws Exception {
		paused.pause(player, song);
		verify(player).setState(any(PlayingSong.class));
	}

}
