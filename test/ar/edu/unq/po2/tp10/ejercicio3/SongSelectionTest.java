package ar.edu.unq.po2.tp10.ejercicio3;

import static org.junit.Assert.assertThrows;
import static org.mockito.Mockito.*;
import org.junit.jupiter.api.Test;

class SongSelectionTest {

	Song song = mock(Song.class);
	Mp3Player player = mock(Mp3Player.class);
	SongSelection songSelection = new SongSelection();
	
	// Test play
	@Test
	void testPlay() throws Exception {
		songSelection.play(player, song);
		verify(player).setState(any(PlayingSong.class));
	}
	
	// Test stop
	@Test
	void testStop() {
		songSelection.stop(player, song);
		verifyNoInteractions(player);
	}
	
	// Test pause
	@Test
	void testPause() throws Exception {
		assertThrows(Exception.class, () -> { songSelection.pause(player, song); });
	}

}
