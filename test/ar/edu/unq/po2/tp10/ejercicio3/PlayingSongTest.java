package ar.edu.unq.po2.tp10.ejercicio3;

import static org.junit.Assert.assertThrows;
import static org.mockito.Mockito.*;

import org.junit.jupiter.api.Test;

class PlayingSongTest {

	Song song = mock(Song.class);
	Mp3Player player = mock(Mp3Player.class);
	PlayingSong playingSong = new PlayingSong();
	
	// Test play
	@Test
	void testPlay() throws Exception {
		assertThrows(Exception.class, () -> { playingSong.play(player, song); });
	}
	
	// Test stop
	@Test
	void testStop() {
		playingSong.stop(player, song);
		verify(player).setState(any(SongSelection.class));
	}
	
	// Test pause
	@Test
	void testPause() throws Exception {
		playingSong.pause(player, song);
		verify(player).setState(any(Paused.class));
	}

}
