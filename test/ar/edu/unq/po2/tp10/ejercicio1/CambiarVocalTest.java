package ar.edu.unq.po2.tp10.ejercicio1;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;

class CambiarVocalTest {

	CambiarVocal codigo = new CambiarVocal();
	
	@Test
	void testEncriptar() {
		assertEquals("hule cumu istes", codigo.encriptar("hola como estas"));
	}
	
	@Test
	void testDesencriptar() {
		assertEquals("hola como estas", codigo.desencriptar("hule cumu istes"));
	}

}
