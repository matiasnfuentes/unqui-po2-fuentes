package ar.edu.unq.po2.tp10.ejercicio1;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;



class CodigoNumericoTest {

	CodigoNumerico codigo = new CodigoNumerico();
	
	@Test
	void testEncriptar() {
		assertEquals("4,9,5,7,15", codigo.encriptar("dIeGo"));
	}
	
	@Test
	void testDesencriptar() {
		assertEquals("diego", codigo.desencriptar("4,9,5,7,15"));
	}

}
