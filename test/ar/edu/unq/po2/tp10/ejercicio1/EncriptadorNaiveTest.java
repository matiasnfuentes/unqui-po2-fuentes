package ar.edu.unq.po2.tp10.ejercicio1;

import static org.mockito.Mockito.*;
import org.junit.jupiter.api.Test;


class EncriptadorNaiveTest {

	ModoDeEncriptacion modo = mock(ModoDeEncriptacion.class);
	EncriptadorNaive encriptador = new EncriptadorNaive(modo);

	@Test
	void testEncriptar() {
		encriptador.encriptar("asd");
		verify(modo).encriptar("asd");
	}
	
	@Test
	void testDesencriptar() {
		encriptador.desencriptar("asd");
		verify(modo).desencriptar("asd");
	}


}
