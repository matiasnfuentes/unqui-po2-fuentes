package ar.edu.unq.po2.tp8.ejercicio6.jugadaDePoquer;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

import org.junit.jupiter.api.Test;

import ar.edu.unq.po2.tp8.ejercicio4.CartaDePoquer;
import ar.edu.unq.po2.tp8.ejercicio4.PokerStatusV2;
import ar.edu.unq.po2.tp8.ejercicio4.jugadaDePoquer.Trio;

public class TrioTest {
	
	// Set up
	CartaDePoquer carta1 = mock(CartaDePoquer.class);
	CartaDePoquer carta2 = mock(CartaDePoquer.class);
	CartaDePoquer carta3 = mock(CartaDePoquer.class);
	CartaDePoquer carta4 = mock(CartaDePoquer.class);
	CartaDePoquer carta5 = mock(CartaDePoquer.class);
	PokerStatusV2 pStatus = mock(PokerStatusV2.class);
	
	// SUT
	Trio trio = new Trio();
	
	// Hay trio
		@Test
		void hayTrio() {
			// Set up
			when(carta1.esIgualA(carta1)).thenReturn(true);
			when(carta2.esIgualA(carta1)).thenReturn(true);
			when(carta3.esIgualA(carta1)).thenReturn(true);
			
			// Verify
			assertEquals("Trio", trio.verificarJugada(carta1, carta2, carta3, carta4, carta5, pStatus));
		}
		
		// no hay trio
		@Test
		void noHayTrio() {
			assertEquals("Nada", trio.verificarJugada(carta1, carta2, carta3, carta4, carta5, pStatus));
		}

}
