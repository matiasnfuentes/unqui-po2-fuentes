package ar.edu.unq.po2.tp8.ejercicio6.jugadaDePoquer;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;
import org.junit.jupiter.api.Test;
import ar.edu.unq.po2.tp8.ejercicio4.CartaDePoquer;
import ar.edu.unq.po2.tp8.ejercicio4.PokerStatusV2;
import ar.edu.unq.po2.tp8.ejercicio4.jugadaDePoquer.Poquer;

class PoquerTest {
	
	// Set up
	CartaDePoquer carta1 = mock(CartaDePoquer.class);
	CartaDePoquer carta2 = mock(CartaDePoquer.class);
	CartaDePoquer carta3 = mock(CartaDePoquer.class);
	CartaDePoquer carta4 = mock(CartaDePoquer.class);
	CartaDePoquer carta5 = mock(CartaDePoquer.class);
	PokerStatusV2 pStatus = mock(PokerStatusV2.class);
	
	// Sut
	Poquer poquer = new Poquer();
	
	// Hay poquer
	@Test
	void hayPoquer() {
		// Set up
		when(carta1.esIgualA(carta1)).thenReturn(true);
		when(carta2.esIgualA(carta1)).thenReturn(true);
		when(carta3.esIgualA(carta1)).thenReturn(true);
		when(carta4.esIgualA(carta1)).thenReturn(true);
		
		// Verify
		assertEquals("Poquer", poquer.verificarJugada(carta1, carta2, carta3, carta4, carta5, pStatus));
	}
	
	// no hay poquer
	@Test
	void noHayPoquer() {
		// Verify
		assertEquals("Nada", poquer.verificarJugada(carta1, carta2, carta3, carta4, carta5, pStatus));
	}

}
