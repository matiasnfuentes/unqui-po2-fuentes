package ar.edu.unq.po2.tp8.ejercicio6.jugadaDePoquer;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;
import org.junit.jupiter.api.Test;
import ar.edu.unq.po2.tp8.ejercicio4.CartaDePoquer;
import ar.edu.unq.po2.tp8.ejercicio4.PokerStatusV2;
import ar.edu.unq.po2.tp8.ejercicio4.jugadaDePoquer.Color;

class ColorTest {
	
	// Set up
	CartaDePoquer carta1 = mock(CartaDePoquer.class);
	CartaDePoquer carta2 = mock(CartaDePoquer.class);
	CartaDePoquer carta3 = mock(CartaDePoquer.class);
	CartaDePoquer carta4 = mock(CartaDePoquer.class);
	CartaDePoquer carta5 = mock(CartaDePoquer.class);
	PokerStatusV2 pStatus = mock(PokerStatusV2.class);
		
	// SUT
	Color color = new Color();

	// Hay Color
	@Test
	void hayColor() {
		// Set up
		when(carta1.poseeMismoPalo(carta1)).thenReturn(true);
		when(carta2.poseeMismoPalo(carta1)).thenReturn(true);
		when(carta3.poseeMismoPalo(carta1)).thenReturn(true);
		when(carta4.poseeMismoPalo(carta1)).thenReturn(true);
		when(carta5.poseeMismoPalo(carta1)).thenReturn(true);
		
		// Verify
		assertEquals("Color", color.verificarJugada(carta1, carta2, carta3, carta4, carta5, pStatus));

	}
	
	// no hay Color
	@Test
	void noHayColor() {
		// Verify
		assertEquals("Nada", color.verificarJugada(carta1, carta2, carta3, carta4, carta5, pStatus));
	}

}
