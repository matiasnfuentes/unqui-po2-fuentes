package ar.edu.unq.po2.tp8.ejercicio6;

import org.junit.jupiter.api.Test;
import static org.mockito.Mockito.*;
import java.util.List;
import ar.edu.unq.po2.tp8.ejercicio4.CartaDePoquer;
import ar.edu.unq.po2.tp8.ejercicio4.PokerStatusV2;
import ar.edu.unq.po2.tp8.ejercicio4.jugadaDePoquer.Color;
import ar.edu.unq.po2.tp8.ejercicio4.jugadaDePoquer.JugadaDePoquer;
import ar.edu.unq.po2.tp8.ejercicio4.jugadaDePoquer.Poquer;
import ar.edu.unq.po2.tp8.ejercicio4.jugadaDePoquer.Trio;

class ManoDePoquerTest {
	
	// Set up 
	CartaDePoquer carta1 = mock(CartaDePoquer.class);
	CartaDePoquer carta2 = mock(CartaDePoquer.class);
	CartaDePoquer carta3 = mock(CartaDePoquer.class);
	CartaDePoquer carta4 = mock(CartaDePoquer.class);
	CartaDePoquer carta5 = mock(CartaDePoquer.class);
	JugadaDePoquer trio = mock(Trio.class);
	JugadaDePoquer poquer = mock(Poquer.class);
	JugadaDePoquer color = mock(Color.class);
	List<JugadaDePoquer> jugadas = List.of(trio,poquer,color);
	// Mano mano1 = mock(Mano.class);
	// Mano mano2 = mock(Mano.class);
	
	// SUT
	PokerStatusV2 pStatus = new PokerStatusV2(jugadas);
	
	// 
	@Test
	void verificar() {
		
		//Verify 
		verify(trio).verificarJugada(carta1, carta2, carta3, carta4, carta5, pStatus);
		verify(color).verificarJugada(carta1, carta2, carta3, carta4, carta5, pStatus);
		verify(poquer).verificarJugada(carta1, carta2, carta3, carta4, carta5, pStatus);
	}
	
}