package ar.edu.unq.po2.tp8.ejercicio2;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class PokerStatusTest {
	PokerStatus pStatus = new PokerStatus(); // SUT

	@Test
	void verificarHayPokerConCuatroCartasIgualesSeguidas() {
		
		//Set Up
		String carta1 = "1C";
		String carta2 = "1P";
		String carta3 = "1D";
		String carta4 = "1T";
		String carta5 = "QC";
		
		//Excercise
		boolean statusObtenido = pStatus.verificar(carta1,carta2,carta3,carta4,carta5);
		
		//Verify 
		assertTrue(statusObtenido);
	}
	
	@Test
	void verificarHayPokerConCuatroCartasIgualesSeguidasDespuesDeLaPrimera() {
		//Set Up
		String carta1 = "QC";
		String carta2 = "10P";
		String carta3 = "10D";
		String carta4 = "10T";
		String carta5 = "10C";
		
		//Excercise
		boolean statusObtenido = pStatus.verificar(carta1,carta2,carta3,carta4,carta5);
		
		//Verify
		assertTrue(statusObtenido);
	}
	
	@Test
	void verificarHayPokerConCartasIgualesEnOrdenAleatorio() {
		//Set Up
		String carta1 = "3C";
		String carta2 = "3P";
		String carta3 = "10D";
		String carta4 = "3T";
		String carta5 = "3C";
		
		//Excercise
		boolean statusObtenido = pStatus.verificar(carta1,carta2,carta3,carta4,carta5);
		
		//Verify
		assertTrue(statusObtenido);
	}
	
	@Test
	void verificarHayPokerConCartasIgualesEnOrdenAleatorioCaso2() {
		//Set Up
		String carta1 = "2C";
		String carta2 = "2P";
		String carta3 = "2D";
		String carta4 = "3T";
		String carta5 = "2C";
		
		//Excercise
		boolean statusObtenido = pStatus.verificar(carta1,carta2,carta3,carta4,carta5);
		
		//Verify
		assertTrue(statusObtenido);
	}
	
	@Test
	void verificarNoHayPoker() {
		
		//Set Up
		String carta1 = "10D";
		String carta2 = "2P";
		String carta3 = "2D";
		String carta4 = "10T";
		String carta5 = "10C";
		
		//Excercise
		boolean statusObtenido = pStatus.verificar(carta1,carta2,carta3,carta4,carta5);
		
		//Verify
		assertFalse(statusObtenido);
	}

}
