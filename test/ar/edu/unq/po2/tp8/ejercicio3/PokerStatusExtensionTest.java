package ar.edu.unq.po2.tp8.ejercicio3;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class PokerStatusExtensionTest {

	PokerStatusExtension pStatus = new PokerStatusExtension(); // SUT

	@Test
	void hayPoquer() {
		
		//Set Up
		String carta1 = "1C";
		String carta2 = "1P";
		String carta3 = "1D";
		String carta4 = "1T";
		String carta5 = "QC";
		
		//Excercise
		String statusObtenido = pStatus.verificar(carta1,carta2,carta3,carta4,carta5);
		
		//Verify 
		assertEquals("Poquer",statusObtenido);
	}
	
	@Test
	void hayColor() {
		
		//Set Up
		String carta1 = "1C";
		String carta2 = "2C";
		String carta3 = "3C";
		String carta4 = "4C";
		String carta5 = "QC";
		
		//Excercise
		String statusObtenido = pStatus.verificar(carta1,carta2,carta3,carta4,carta5);
		
		//Verify 
		assertEquals("Color",statusObtenido);
	}
	
	@Test
	void hayTrio() {
		
		//Set Up
		String carta1 = "2C";
		String carta2 = "2D";
		String carta3 = "2D";
		String carta4 = "4C";
		String carta5 = "4C";
		
		//Excercise
		String statusObtenido = pStatus.verificar(carta1,carta2,carta3,carta4,carta5);
		
		//Verify 
		assertEquals("Trio",statusObtenido);
	}
	
	@Test
	void hayTrioSegundoCaso() {
		
		//Set Up
		String carta1 = "1C";
		String carta2 = "10D";
		String carta3 = "10C";
		String carta4 = "4C";
		String carta5 = "10D";
		
		//Excercise
		String statusObtenido = pStatus.verificar(carta1,carta2,carta3,carta4,carta5);
		
		//Verify 
		assertEquals("Trio",statusObtenido);
	}
	
	@Test
	void noHayNada() {
		
		//Set Up
		String carta1 = "1P";
		String carta2 = "2C";
		String carta3 = "10C";
		String carta4 = "4P";
		String carta5 = "2C";
		
		//Excercise
		String statusObtenido = pStatus.verificar(carta1,carta2,carta3,carta4,carta5);
		
		//Verify 
		assertEquals("Nada",statusObtenido);
	}

}
