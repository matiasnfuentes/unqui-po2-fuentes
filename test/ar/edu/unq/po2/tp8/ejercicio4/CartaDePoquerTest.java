package ar.edu.unq.po2.tp8.ejercicio4;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;


class CartaDePoquerTest {
	
	// Set up
	CartaDePoquer cuatroC = new CartaDePoquer(ValorDeCarta.CUATRO,Palo.Corazones);
	CartaDePoquer cuatroP = new CartaDePoquer(ValorDeCarta.CUATRO,Palo.Picas); 
	CartaDePoquer cincoP = new CartaDePoquer(ValorDeCarta.CINCO,Palo.Picas); 

	@Test
	void testGetValor() {
		
		// Excercise
		ValorDeCarta valor = cuatroC.getValor();
		
		// Verify
		assertEquals(valor, ValorDeCarta.CUATRO);
	}
	
	@Test
	void testGetPalo() {
		
		// Excercise
		Palo palo = cuatroC.getPalo();
		
		// Verify
		assertEquals(palo, Palo.Corazones);
	}
	
	@Test
	void compararPaloCorrecto() {
		
		// Excercise
		boolean comparacion = cuatroP.poseeMismoPalo(cincoP);
		
		// Verify
		assertTrue(comparacion);
	}
	
	@Test
	void compararPaloIncorrecto() {
		
		// Excercise
		boolean comparacion = cuatroP.poseeMismoPalo(cuatroC);
		
		// Verify
		assertFalse(comparacion);
	}
	
	@Test
	void compararValorCorrecto() {
		
		// Excercise
		boolean comparacion = cincoP.esMayorQue(cuatroP);
		
		// Verify
		assertTrue(comparacion);
	}
	
	@Test
	void compararValorIncorrecto() {
		
		// Excercise
		boolean comparacion = cuatroP.esMayorQue(cincoP);
		
		// Verify
		assertFalse(comparacion);
	}

}
