package ar.edu.unq.po2.tp8.ejercicio4;

import static org.junit.jupiter.api.Assertions.*;

import java.util.List;

import org.junit.jupiter.api.Test;
import ar.edu.unq.po2.tp8.ejercicio4.jugadaDePoquer.JugadaDePoquer;
import ar.edu.unq.po2.tp8.ejercicio4.jugadaDePoquer.Poquer;
import ar.edu.unq.po2.tp8.ejercicio4.jugadaDePoquer.Trio;
import ar.edu.unq.po2.tp8.ejercicio4.jugadaDePoquer.Color;

class PokerStatusV2Test {
	
	// Set up
	JugadaDePoquer trio = new Trio();
	JugadaDePoquer poquer = new Poquer();
	JugadaDePoquer color = new Color();
	List<JugadaDePoquer> jugadas = List.of(trio,poquer,color);
	
	// SUT
	PokerStatusV2 pStatus = new PokerStatusV2(jugadas);

	@Test
	void hayPoquer() {
		
		//Set Up
		CartaDePoquer carta1 = new CartaDePoquer(ValorDeCarta.AS,Palo.Corazones);
		CartaDePoquer carta2 = new CartaDePoquer(ValorDeCarta.AS,Palo.Picas);
		CartaDePoquer carta3 = new CartaDePoquer(ValorDeCarta.AS,Palo.Diamantes);
		CartaDePoquer carta4 = new CartaDePoquer(ValorDeCarta.AS,Palo.Treboles);
		CartaDePoquer carta5 = new CartaDePoquer(ValorDeCarta.Q,Palo.Corazones);
		
		//Excercise
		String statusObtenido = pStatus.verificar(carta1,carta2,carta3,carta4,carta5);
		
		//Verify 
		assertEquals("Poquer",statusObtenido);
	}
	
	@Test
	void hayColor() {
		
		//Set Up
		CartaDePoquer carta1 = new CartaDePoquer(ValorDeCarta.AS,Palo.Corazones);
		CartaDePoquer carta2 = new CartaDePoquer(ValorDeCarta.DOS,Palo.Corazones);
		CartaDePoquer carta3 = new CartaDePoquer(ValorDeCarta.TRES,Palo.Corazones);
		CartaDePoquer carta4 = new CartaDePoquer(ValorDeCarta.CUATRO,Palo.Corazones);
		CartaDePoquer carta5 = new CartaDePoquer(ValorDeCarta.Q,Palo.Corazones);

		//Excercise
		String statusObtenido = pStatus.verificar(carta1,carta2,carta3,carta4,carta5);
		
		//Verify 
		assertEquals("Color",statusObtenido);
	}
	
	@Test
	void hayTrio() {
		
		//Set Up
		CartaDePoquer carta1 = new CartaDePoquer(ValorDeCarta.DOS,Palo.Corazones);
		CartaDePoquer carta2 = new CartaDePoquer(ValorDeCarta.DOS,Palo.Diamantes);
		CartaDePoquer carta3 = new CartaDePoquer(ValorDeCarta.DOS,Palo.Treboles);
		CartaDePoquer carta4 = new CartaDePoquer(ValorDeCarta.CUATRO,Palo.Corazones);
		CartaDePoquer carta5 = new CartaDePoquer(ValorDeCarta.CUATRO,Palo.Treboles);
		
		//Excercise
		String statusObtenido = pStatus.verificar(carta1,carta2,carta3,carta4,carta5);
		
		//Verify 
		assertEquals("Trio",statusObtenido);
	}
	
	@Test
	void hayTrioSegundoCaso() {
		
		//Set Up
		CartaDePoquer carta1 = new CartaDePoquer(ValorDeCarta.AS,Palo.Corazones);
		CartaDePoquer carta2 = new CartaDePoquer(ValorDeCarta.DIEZ,Palo.Diamantes);
		CartaDePoquer carta3 = new CartaDePoquer(ValorDeCarta.DIEZ,Palo.Corazones);
		CartaDePoquer carta4 = new CartaDePoquer(ValorDeCarta.CUATRO,Palo.Corazones);
		CartaDePoquer carta5 = new CartaDePoquer(ValorDeCarta.DIEZ,Palo.Diamantes);
		
		//Excercise
		String statusObtenido = pStatus.verificar(carta1,carta2,carta3,carta4,carta5);
		
		//Verify 
		assertEquals("Trio",statusObtenido);
	}
	
	@Test
	void noHayNada() {
		
		//Set Up
		CartaDePoquer carta1 = new CartaDePoquer(ValorDeCarta.AS,Palo.Picas);
		CartaDePoquer carta2 = new CartaDePoquer(ValorDeCarta.DOS,Palo.Corazones);
		CartaDePoquer carta3 = new CartaDePoquer(ValorDeCarta.DIEZ,Palo.Corazones);
		CartaDePoquer carta4 = new CartaDePoquer(ValorDeCarta.CUATRO,Palo.Picas);
		CartaDePoquer carta5 = new CartaDePoquer(ValorDeCarta.DOS,Palo.Corazones);
		
		//Excercise
		String statusObtenido = pStatus.verificar(carta1,carta2,carta3,carta4,carta5);
		
		//Verify 
		assertEquals("Nada",statusObtenido);
	}

}
