package ar.edu.unq.po2.tp3;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;


public class RectanguloTestCase{
	
	private Rectangulo rectangulo;
	
	@Before 
	public void setUp() throws Exception{
		
		//Se crea el rectangulo
		rectangulo = new Rectangulo (new Point(4,4),10,20);
	}
	
	@Test 
	
	public void testArea() {
		
		// Calcula el area del rectagulo
		float area = rectangulo.area();
		
		// Verifica que el area sea la indicada
		assertEquals(area, 200,0.001);
	}

	@Test 
	
	public void testperimetro() {
		
		// calcula el perimetro
		float perimetro = rectangulo.perimetro();
		
		// Verifica que el perimetro sea el indicado
		assertEquals(perimetro, 60,0.001);
	}

	
	@Test 
	
	public void testHorizontal() {
		
		// Calcula si el rectangulo es horizontal
		boolean horizontal = rectangulo.esHorizontal();
		
		// Verifica los valores obtenidos
		assertEquals(horizontal, false);
	}
}