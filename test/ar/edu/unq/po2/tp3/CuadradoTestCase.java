package ar.edu.unq.po2.tp3;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;


public class CuadradoTestCase{
	
	private Cuadrado cuadrado;
	
	@Before 
	public void setUp() throws Exception{
		
		//Se crea el cuadrado
		cuadrado = new Cuadrado (new Point(4,4),10);
	}
	
	@Test 
	
	public void testArea() {
		
		// Calcula el area del rectagulo
		float area = cuadrado.area();
		
		// Verifica que el area sea la indicada
		assertEquals(area, 100,0.001);
	}

	@Test 
	
	public void testperimetro() {
		
		// Calcula el perimetro
		float perimetro = cuadrado.perimetro();
		
		// Verifica que el perimetro sea el indicado
		assertEquals(perimetro, 40,0.001);
	}

	
	@Test 
	
	public void testHorizontal() {
		
		// Calcula si cuadrado es horizontal
		boolean horizontal = cuadrado.esHorizontal();
		
		// Verifica los valores obtenidos
		assertEquals(horizontal, false);
	}
}