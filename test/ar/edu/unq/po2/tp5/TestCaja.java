package ar.edu.unq.po2.tp5;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import ar.edu.unq.po2.tp5.laCajaMercadoCentral.Afip;
import ar.edu.unq.po2.tp5.laCajaMercadoCentral.Caja;
import ar.edu.unq.po2.tp5.laCajaMercadoCentral.Cobrable;
import ar.edu.unq.po2.tp5.laCajaMercadoCentral.Impuesto;
import ar.edu.unq.po2.tp5.laCajaMercadoCentral.Producto;
import ar.edu.unq.po2.tp5.laCajaMercadoCentral.ProductoCooperativa;
import ar.edu.unq.po2.tp5.laCajaMercadoCentral.Servicio;

class TestCaja {
	
	Caja caja = new Caja();
	
	//Procuctos
	Producto zanahoria = new Producto(5,5.50);
	ProductoCooperativa tela = new ProductoCooperativa (10, 10.50);
	
	// Servicios 
	Afip afip = new Afip();
	Servicio agua = new Servicio(afip,9.75,10);
	Impuesto abl = new Impuesto(afip,175.75);
	
	// CanastaDeProductos
	ArrayList<Cobrable> canastaDeProductos = new ArrayList<Cobrable>();
	
	@BeforeEach
	void setUp() throws Exception {
		canastaDeProductos.add(tela);
		canastaDeProductos.add(zanahoria);
		canastaDeProductos.add(abl);
		canastaDeProductos.add(agua);
	}

	@Test
	void test() {
		double montoAPagar = caja.cobrarConceptos(canastaDeProductos);
		assertEquals(288.20, montoAPagar, 0.01);
	}

}
