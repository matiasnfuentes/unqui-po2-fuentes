package ar.edu.unq.po2.tp5;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import ar.edu.unq.po2.tp5.laCajaMercadoCentral.Producto;
import ar.edu.unq.po2.tp5.laCajaMercadoCentral.ProductoCooperativa;

class TestProducto {
	
	Producto zanahoria = new Producto(5,5.50);
	ProductoCooperativa tela = new ProductoCooperativa (10, 10.50);

	@BeforeEach
	void setUp() throws Exception {
	}

	@Test
	void testRegistrarZanahoria() {
		zanahoria.registrar();
		assertEquals(4, zanahoria.getStock());
	}
	
	@Test
	void testRegistrarZanahoria2() {
		double precioEsperado = zanahoria.registrar();
		assertEquals(5.50, precioEsperado, 0.01);
	}
	
	@Test
	void testRegistrarTela() {
		tela.registrar();
		assertEquals(9, tela.getStock());
	}

	@Test
	void testRegistrarTela2() {
		double precioEsperado = tela.registrar();
		assertEquals(9.45, precioEsperado, 0.01);
	}

}
