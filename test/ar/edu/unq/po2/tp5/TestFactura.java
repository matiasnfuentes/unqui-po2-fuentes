package ar.edu.unq.po2.tp5;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import ar.edu.unq.po2.tp5.laCajaMercadoCentral.Afip;
import ar.edu.unq.po2.tp5.laCajaMercadoCentral.Impuesto;
import ar.edu.unq.po2.tp5.laCajaMercadoCentral.Servicio;

class TestFactura {
	
	Afip afip = new Afip();
	Servicio agua = new Servicio(afip,9.75,10);
	Impuesto abl = new Impuesto(afip,175.75);
	
	@BeforeEach
	void setUp() throws Exception {
	}

	@Test
	void RegistrarAgua() {
		double montoEsperado = agua.registrar();
		assertEquals(97.5, montoEsperado, 0.01);
	}
	
	@Test
	void RegistrarAbl() {
		double montoEsperado = abl.registrar();
		assertEquals(175.75, montoEsperado, 0.01);
	}

}
