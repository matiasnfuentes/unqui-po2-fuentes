package ar.edu.unq.po2.tp6.banco;

import static org.junit.jupiter.api.Assertions.*;

import java.time.LocalDate;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import ar.edu.unq.po2.tp6.bancoYPrestamos.Banco;
import ar.edu.unq.po2.tp6.bancoYPrestamos.Cliente;
import ar.edu.unq.po2.tp6.bancoYPrestamos.Propiedad;
import ar.edu.unq.po2.tp6.bancoYPrestamos.SolicitudCreditoHipotecario;
import ar.edu.unq.po2.tp6.bancoYPrestamos.SolicitudCreditoPersonal;

class CalculoCrediticioTestCase {
	
	Banco galicia = new Banco();
	
	// Garantia
	Propiedad casa = new Propiedad("Casa","Calle falasa 123",1000000);
	
	//Cliente Matias 
	private Cliente matias = new Cliente("Fuentes","Matias","Calle falsa 123",LocalDate.of(1992, 3, 24),100000);
	
	
	// Solicitud valida		
	private SolicitudCreditoHipotecario solicitudMatiasValida
						= new SolicitudCreditoHipotecario(matias,42500,10,casa);
	
	// Solicitud valida2		
		private SolicitudCreditoPersonal solicitudMatiasValida2
								= new SolicitudCreditoPersonal(matias,42500,10);
		
	// Solicitud invalida por monto de cuota
	private SolicitudCreditoPersonal solicitudMatiasInvalidaPorCuota
								= new SolicitudCreditoPersonal(matias,300000,4);
	
	// Solicitud invalida por monto de cuota
	private SolicitudCreditoHipotecario solicitudMatiasInvalidaPorCuota2
								= new SolicitudCreditoHipotecario(matias,200000,3,casa);
	
	// Solicitud invalida por monto total (supera el 70 de la garantia)
	private SolicitudCreditoHipotecario solicitudMatiasInvalidaPorGarantia
								= new SolicitudCreditoHipotecario(matias,800000,50,casa);
	
	// Cliente Pepe 
	
	private Cliente pepe = new Cliente("Estaviejo","Pepe","Calle falsa 123",LocalDate.of(1955, 10, 2),200000);
	
	// Solicitud valida
	private SolicitudCreditoHipotecario solicitudPepeCorrecta 
								= new SolicitudCreditoHipotecario(pepe,100,10,casa);
	
	// Solicitud invalida por edad
	private SolicitudCreditoHipotecario solicitudPepeImpagable
								= new SolicitudCreditoHipotecario(pepe,100,15,casa);	
	
	@BeforeEach
	void setUp() throws Exception {
		galicia.registrarSolicitud(solicitudMatiasInvalidaPorCuota);
		galicia.registrarSolicitud(solicitudMatiasInvalidaPorCuota2);
		galicia.registrarSolicitud(solicitudMatiasInvalidaPorGarantia);
		galicia.registrarSolicitud(solicitudMatiasValida);
		galicia.registrarSolicitud(solicitudMatiasValida2);
		galicia.registrarSolicitud(solicitudPepeCorrecta);
		galicia.registrarSolicitud(solicitudPepeImpagable);
	}

	@Test
	void test() {
		assertEquals(85100,galicia.montoADesembolsar());
	}

}
