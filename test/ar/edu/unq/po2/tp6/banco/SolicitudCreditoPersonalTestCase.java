package ar.edu.unq.po2.tp6.banco;

import static org.junit.jupiter.api.Assertions.*;
import java.time.LocalDate;
import org.junit.jupiter.api.Test;
import ar.edu.unq.po2.tp6.bancoYPrestamos.Cliente;
import ar.edu.unq.po2.tp6.bancoYPrestamos.SolicitudCreditoPersonal;

class SolicitudCreditoPersonalTestCase {
	
	// Para que sea valido, debe tener ingresos anuales de al menos $15000
	// y el monto de la cuota no debe superar el 70 % de sus ingresos mensuales.
		
	//Cliente Matias 
	private Cliente matias = new Cliente("Fuentes","Matias","Calle falsa 123",LocalDate.of(1992, 3, 24),100000);
	
	// Solicitud valida		
	private SolicitudCreditoPersonal solicitudMatiasValida
							= new SolicitudCreditoPersonal(matias,42500,10);
		
	// Solicitud invalida por monto de cuota
	private SolicitudCreditoPersonal solicitudMatiasInvalidaPorCuota
							= new SolicitudCreditoPersonal(matias,300000,4);
	
	// Cliente Pepe 
	private Cliente pepe = new Cliente("Estaviejo","Pepe","Calle falsa 123",LocalDate.of(1955, 10, 2),1000);
		
	// Solicitud invalida por sueldoAnual
	private SolicitudCreditoPersonal solicitudPepeInvalidaPorSueldoAnual
							= new SolicitudCreditoPersonal(pepe,100,10);
	
	@Test
	void testSolicitudMatiasValida() {
		assertTrue(solicitudMatiasValida.chequearSolicitud());
	}
	
	@Test
	void testSolicitudMatiasInvalidaPorCuota() {
		assertFalse(solicitudMatiasInvalidaPorCuota.chequearSolicitud());
	}
	
	@Test
	void testSolicitudPepeInvalidaPorSueldoAnual() {
		assertFalse(solicitudPepeInvalidaPorSueldoAnual.chequearSolicitud());
	}

	@Test
	void testGetMonto() {
		assertEquals(42500, solicitudMatiasValida.getMonto(),0.01);
	}
	
	@Test
	void testCuotaMensual() {
		assertEquals(4250, solicitudMatiasValida.cuotaMensual(),0.01);
	}
	
}
