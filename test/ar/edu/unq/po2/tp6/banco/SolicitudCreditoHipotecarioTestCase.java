package ar.edu.unq.po2.tp6.banco;


import static org.junit.jupiter.api.Assertions.*;
import java.time.LocalDate;
import org.junit.jupiter.api.Test;
import ar.edu.unq.po2.tp6.bancoYPrestamos.SolicitudCreditoHipotecario;
import ar.edu.unq.po2.tp6.bancoYPrestamos.Propiedad;
import ar.edu.unq.po2.tp6.bancoYPrestamos.Cliente;

class SolicitudCreditoHipotecarioTestCase {
	
	// Para que sea valido, la cuota no debe superar 50 % de los ingresos mensuales del solicitante
	// y el monto total no debe superar el 70 % de la garantia.
	
	//Cliente Matias 
	private Cliente matias = new Cliente("Fuentes","Matias","Calle falsa 123",LocalDate.of(1992, 3, 24),100000);
	
	Propiedad casa = new Propiedad("Casa","Calle falasa 123",1000000);
	
	// Solicitud valida		
	private SolicitudCreditoHipotecario solicitudMatiasValida
						= new SolicitudCreditoHipotecario(matias,42500,10,casa);
	
	// Solicitud invalida por monto de cuota
	private SolicitudCreditoHipotecario solicitudMatiasInvalidaPorCuota
	= new SolicitudCreditoHipotecario(matias,200000,3,casa);
	
	// Solicitud invalida por monto total (supera el 70 de la garantia)
		private SolicitudCreditoHipotecario solicitudMatiasInvalidaPorGarantia
		= new SolicitudCreditoHipotecario(matias,800000,50,casa);
	
	// Cliente Pepe 
	
	private Cliente pepe = new Cliente("Estaviejo","Pepe","Calle falsa 123",LocalDate.of(1955, 10, 2),200000);
	
	// Solicitud valida
	private SolicitudCreditoHipotecario solicitudPepeCorrecta 
						= new SolicitudCreditoHipotecario(pepe,100,10,casa);
	
	// Solicitud invalida por edad
	private SolicitudCreditoHipotecario solicitudPepeImpagable
						= new SolicitudCreditoHipotecario(pepe,100,15,casa);


	@Test
	void testSolicitudMatiasValida() {
		assertTrue(solicitudMatiasValida.chequearSolicitud());
	}
	
	@Test
	void testSolicitudMatiasInvalidaPorCuota() {
		assertFalse(solicitudMatiasInvalidaPorCuota.chequearSolicitud());
	}
	
	@Test
	void testSolicitudMatiasInvalidaPorGarantia() {
		assertFalse(solicitudMatiasInvalidaPorGarantia.chequearSolicitud());
	}
	
	@Test
	void testSolicitudPepeCorrecta() {
		assertTrue(solicitudPepeCorrecta.chequearSolicitud());
	}
	
	@Test
	void testSolicitudPepeImpagable() {
		assertFalse(solicitudPepeImpagable.chequearSolicitud());
	}
	
	@Test
	void testGetMonto() {
		assertEquals(42500, solicitudMatiasValida.getMonto(),0.01);
	}
	
	@Test
	void testCuotaMensual() {
		assertEquals(4250, solicitudMatiasValida.cuotaMensual(),0.01);
	}
	

}
