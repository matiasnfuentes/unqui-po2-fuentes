package ar.edu.unq.po2.tp9.observer.ejercicio2;

import static org.mockito.Mockito.*;
import java.util.ArrayList;
import org.junit.jupiter.api.Test;

class AplicacionTest {

	ArrayList<Partido> partidos = spy(new ArrayList<Partido>());
	Partido partido = mock(Partido.class);
	ArrayList<ObservadorDeInteres> observadores = new ArrayList<ObservadorDeInteres>();
	Aplicacion app = new Aplicacion(partidos,observadores);

	//Test agregarPartido
	@Test
	void testAgregarPartido() {
		app.agregarPartido(partido);
		verify(partidos).add(partido);
	}

}
