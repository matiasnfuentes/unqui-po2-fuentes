package ar.edu.unq.po2.tp9.observer.ejercicio2;
import static org.mockito.Mockito.*;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class PartidoTest {

	ArrayList<String> contrincantes = new ArrayList<String>(List.of("Boca","River"));
	Partido partido = new Partido("2-1",contrincantes,"Futbol");

	//Test getResultado
	@Test
	void testGetResultado() {
		assertEquals("2-1", partido.getResultado());
	}
	
	//Test getContrincantes
	@Test
	void testGetContrincantes() {
		ArrayList<String> contrincantes = partido.getContrincantes();
		assertTrue(contrincantes.containsAll(List.of("Boca","River")));
	}
	
	//Test getDeporte
	@Test
	void testGetDeporte() {
		assertEquals("Futbol",partido.getDeporte());
	}

}
