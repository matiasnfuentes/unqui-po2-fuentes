package ar.edu.unq.po2.tp9.observer.ejercicio2;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;
import java.util.ArrayList;
import java.util.List;
import org.junit.jupiter.api.Test;

class InteresTest {

	ArrayList<String> deportes = spy(new ArrayList<String>(List.of("Tenis","Futbol")));
	ArrayList<String> contrincantes = spy(new ArrayList<String>(List.of("Boca","River")));
	Interes interes = new Interes(deportes,contrincantes); 

	
	@Test
	void testGetContrincantes() {
		ArrayList<String> contrincantes = interes.getContrincantes();
		assertTrue(contrincantes.containsAll(List.of("Boca","River")));
	}
	
	@Test
	void testGetDeportes() {
		ArrayList<String> deportes = interes.getDeportes();
		assertTrue(deportes.containsAll(List.of("Tenis","Futbol")));
	}

}
