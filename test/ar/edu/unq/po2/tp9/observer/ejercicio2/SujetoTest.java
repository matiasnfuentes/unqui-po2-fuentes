package ar.edu.unq.po2.tp9.observer.ejercicio2;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

import java.util.ArrayList;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class SujetoTest {

	ArrayList<ObservadorDeInteres> observadores = spy(new ArrayList<ObservadorDeInteres>());
	ObservadorDeInteres observador = mock(ObservadorDeInteres.class);
	Partido partido = mock(Partido.class);
	Sujeto sujeto = new Sujeto(observadores);
	
	//Test agregar observador
	@Test
	void testAgregarObservador() {
		sujeto.agregarObservador(observador);
		verify(observadores).add(observador);	
	}
	
	//Test eliminar observador
	@Test
	void testEliminarObservador() {
		sujeto.eliminarObservador(observador);
		verify(observadores).remove(observador);	
	}
	
	//Test notificarObservadores 
	@Test
	void testNotificarObservadores() {
		sujeto.agregarObservador(observador);
		sujeto.notificarObservadores(partido);
		verify(observador).update(partido);
	}
	


}
