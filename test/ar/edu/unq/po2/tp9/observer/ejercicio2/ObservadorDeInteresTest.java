package ar.edu.unq.po2.tp9.observer.ejercicio2;

import java.util.ArrayList;
import java.util.List;
import static org.mockito.Mockito.*;

import org.junit.jupiter.api.Test;

class ObservadorDeInteresTest {

	Observador observador = mock(Observador.class);
	Interes interes = mock(Interes.class);
	Partido partido = mock(Partido.class);
	ObservadorDeInteres observadorDeInteres = new ObservadorDeInteres(observador,interes);
	
	
	@Test
	void updateExitoso() {
		when(partido.getDeporte()).thenReturn("Futbol");
		when(interes.getDeportes()).thenReturn(new ArrayList<String>(List.of("Futbol")));
		observadorDeInteres.update(partido);
		verify(observador).update(partido);
	}
	
	@Test
	void updateFallido() {
		when(partido.getDeporte()).thenReturn("Tenis");
		when(interes.getDeportes()).thenReturn(new ArrayList<String>(List.of("Futbol")));
		observadorDeInteres.update(partido);
		verify(observador, never()).update(partido);
	}
	

}
