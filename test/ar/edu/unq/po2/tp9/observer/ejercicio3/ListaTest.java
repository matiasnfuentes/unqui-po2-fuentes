package ar.edu.unq.po2.tp9.observer.ejercicio3;

import static org.mockito.Mockito.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import org.junit.jupiter.api.Test;

import ar.edu.unq.po2.tp9.observer.ejercicio3.estadosDeLaPartida.Lista;

class ListaTest {

	Partida partida = mock(Partida.class);
	PreguntaYRespuesta respuesta = mock(PreguntaYRespuesta.class);
	Participante participante = mock(Participante.class);
	
	Lista lista = new Lista();

	@Test
	void testRespuestaIncorrecta() {
		
		Map<Participante,ArrayList<PreguntaYRespuesta>> map =  new HashMap<Participante,ArrayList<PreguntaYRespuesta>>();
		ArrayList<PreguntaYRespuesta> respuestas = new ArrayList<PreguntaYRespuesta>();
		map.put(participante, respuestas);
		when(partida.getParticipantes()).thenReturn(map);
		
		lista.verificarRespuesta(partida,participante, respuesta);
		
		verify(partida).notificarObservador(participante, "Su respuesta es incorrecta");
	}

}
