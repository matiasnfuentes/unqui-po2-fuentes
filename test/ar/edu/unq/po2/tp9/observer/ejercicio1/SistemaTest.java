package ar.edu.unq.po2.tp9.observer.ejercicio1;

import static org.mockito.Mockito.*;
import java.util.ArrayList;
import org.junit.jupiter.api.Test;

class SistemaTest {
	
	ArrayList<Articulo> spyArticulos = spy(new ArrayList<Articulo>());
	Alarma alarma = mock(Alarma.class);
	Sistema sistema = new Sistema(spyArticulos,alarma);


	@Test
	void testCargarPublicacion() {
		Articulo articulo = mock(Articulo.class);
		sistema.cargarArticulo(articulo);
		verify(spyArticulos).add(articulo);
		verify(alarma).notificar(articulo);
	}

}
