package ar.edu.unq.po2.tp9.observer.ejercicio1;

import static org.mockito.Mockito.*;
import java.util.ArrayList;
import org.junit.jupiter.api.Test;

class AlarmaTest {

	ArrayList<Suscripcion> spySuscripciones = spy(new ArrayList<Suscripcion>());
	Suscripcion suscripcion = mock(Suscripcion.class);
	Observador observador = mock(Observador.class);
	Articulo articulo = mock(Articulo.class);
	
	//SUT
	Alarma alarma = new Alarma(spySuscripciones);

	// El observador es alertado ante un articulo de su interes
	@Test
	void testAlertarObservador() {
		//Set up
		alarma.agregarSuscripcion(suscripcion);
		when(suscripcion.compararReferencia(articulo)).thenReturn(true);
		
		//Excercise
		alarma.notificar(articulo);
		
		//Verify
		verify(suscripcion).actualizar(articulo);
	}
	
	// El observador no es alertado ante un articulo que no es de su interes
	@Test
	void testNoAlertarObservador() {
		//Set up
		alarma.agregarSuscripcion(suscripcion);
		when(suscripcion.compararReferencia(articulo)).thenReturn(false);
		
		//Excercise
		alarma.notificar(articulo);
		
		//Verify
		verify(suscripcion, never()).actualizar(articulo);
	}

}
