package ar.edu.unq.po2.tp9.composite.ejercicio1;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;
import static org.mockito.Mockito.*;

class ParcelaTest {
	
	// SUT 
	ParcelaDividida parcela = new ParcelaDividida();
	ParcelaDividida parcela2 = new ParcelaDividida();
	ParcelaDividida parcela3 = new ParcelaDividida();
	
	// DOCs
	
	Soja soja = mock(Soja.class);
	Trigo trigo = mock(Trigo.class);
	

	//test ganancia
	@Test
	void ganancia() {
		// Set  up
		when(soja.ganancia()).thenReturn(500.00);
		when(trigo.ganancia()).thenReturn(300.00);
		when(soja.subparcelas()).thenReturn(1);
		when(trigo.subparcelas()).thenReturn(1);
		
		parcela2.add(trigo);
		parcela2.add(trigo);
		parcela2.add(parcela3);
		
		parcela3.add(soja);
		
		parcela.add(trigo);
		parcela.add(soja);
		parcela.add(soja);
		parcela.add(parcela2);
		double ganancia = 0;
		
		// Excersice
		ganancia = parcela.ganancia();
		
		// Verify
		assertEquals(1481.25,ganancia,0);
		
	}

}
